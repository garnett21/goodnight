FROM registry.cn-hangzhou.aliyuncs.com/aliyun-node/alinode:4.6.0-slim
WORKDIR /app
RUN echo "Asia/Shanghai" > /etc/timezone && dpkg-reconfigure -f noninteractive tzdata && \
    cat /etc/timezone

COPY package.json /app/package.json
RUN npm install --registry=https://registry.npm.taobao.org
COPY . /app
EXPOSE 7001
CMD npm start