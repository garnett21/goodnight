'use strict';
const Controller = require('egg').Controller;

class BaseController extends Controller {
    success(data, message, code, statusCode) {
        this.ctx.body = {
            success: true,
            data: data,
            message: message,
            code: code,
        }
        this.ctx.status = statusCode || 200;
    }

    fail(message, errorCode, statusCode) {
        this.ctx.body = {
            success: false,
            message: message,
            errorCode: errorCode
        }
        this.ctx.status = statusCode || 400;
    }

    socketSuccess(socketId, channel, data, message) {
        const {app} = this;
        let wrappedData = {
            success: true,
            data: data,
            message: message,
        }
        app.io.to(socketId).emit(channel, wrappedData);
    }

    socketFail(socketId, channel, message) {
        const {app} = this;
        let wrappedData = {
            success: false,
            message: message,
        }
        app.io.to(socketId).emit(channel, wrappedData);
    }


    getUnionId() {
        let payload = this.ctx.helper.decodeToken(this.ctx.request.header.authorization);
        return payload.unionId;
    }

    getPlatform(){
        let payload = this.ctx.helper.decodeToken(this.ctx.request.header.authorization);
        return payload.platform; 
    }

    getUnionIdFromSocket(){
        let payload = this.ctx.helper.decodeTokenFromSocket(this.ctx.socket.handshake.query.token);
        return payload.unionId; 
    }

    getPlatformFromSocket(){
        let payload = this.ctx.helper.decodeTokenFromSocket(this.ctx.socket.handshake.query.token);
        return payload.platform; 
    }
}

module.exports = BaseController;
