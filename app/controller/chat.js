'use strict';
const BaseController = require('./base');

class ChatController extends BaseController {
    /**
     * 发送语音信息
     */
    async sendVoiceMessage() {
        const { service, app } = this;
        let senderId = this.getUnionId();
        let platform = this.getPlatform();
        let { receiverId, messageId } = await service.chat.sendVoiceMessage(senderId, platform);
        let receiverSocketId = await app.redis.get(`socket@${receiverId}`);
        if (receiverSocketId) {
            this.socketSuccess(receiverSocketId, app.config.socketChannel.newVoiceMessage, undefined, 'new voice message received');
        }
        this.success({messageId}, 'send voice message success', 200);
    }

    /**
     * 获取已发送的语音信息
     */
    async getSentVoiceMessage() {
        const { service } = this;
        let senderId = this.getUnionId();
        let sentVoiceMessage = await service.chat.getSentVoiceMessage(senderId);
        this.success({ sentVoiceMessage }, 'get sent message success', 200);
    }

    /**
     * 获取已收到的语音信息
     */
    async getReceivedVoiceMessage() {
        const { service } = this;
        let receiverId = this.getUnionId();
        let receivedVoiceMessage = await service.chat.getReceivedVoiceMessage(receiverId);
        this.success({ receivedVoiceMessage }, 'get received message success', 200);
    }

}
module.exports = ChatController;
