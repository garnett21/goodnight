'use strict';
const BaseController = require('./base');

class InviteController extends BaseController {
    /**
     * 获得邀请id与被邀请者信息
     */
    async getInvitation() {
        const { service } = this;
        let unionId = this.getUnionId();
        let invitation = await service.invite.getInvitation(unionId);
        this.success(invitation, 'get invitation success', 200);
    }

}
module.exports = InviteController;
