'use strict';
const BaseController = require('./base');


class LightController extends BaseController {
    //展示目前可用的灯
    async showAvailableLight() {
        let availableLight = await this.service.light.showAvailableLight();
        this.success(availableLight, 'available lights are as follow', 200);
    }
    //查看当前选择的灯
    async showCurrentLight() {
        let unionId = this.getUnionId();
        let showCurrentLight = await this.service.light.showCurrentLight(unionId);
        if (showCurrentLight !== null) {
            let lightId = showCurrentLight.lightId;
            let picurlOfCurrentLight = await this.service.light.picurlOfCurrentLight(lightId);
            this.success({ showCurrentLight, picurlOfCurrentLight }, 'this is the current light', 200);
        } else {
            await this.service.light.changeLight(unionId, 1);
            let picurlOfCurrentLight = await this.service.light.picurlOfCurrentLight(1);
            let currentLightMessage = await this.service.light.showCurrentLight(unionId);
            this.success({
                currentLightMessage, picurlOfCurrentLight
            },
                'a new user with default light', 200);
        };
    };


    //改变灯皮肤
    async changeLight() {
        const { ctx } = this;
        let lightId = ctx.request.body.lightId;
        let unionId = this.getUnionId();
        await this.service.light.changeLight(unionId, lightId);
        this.success('change succeed', 200);
    };

    //把用户关灯的状态和时间写入数据库
    async turnOffLight() {
        const { app } = this;
        let unionId = this.getUnionId();
        let currentMatchId = await this.service.light.turnOffLightTime(unionId);
        if (currentMatchId) {
            let currentMatchSocketId = await app.redis.get(`socket@${currentMatchId}`);
            if (currentMatchSocketId) {
                this.socketSuccess(currentMatchSocketId, app.config.socketChannel.matchLightOff, undefined, 'match light off');
            }
        }
        await this.service.light.turnOffLightStatus(unionId);
        this.success('the light is off', 200);
    };


    //查看我的关灯记录
    async myLightOffTime() {
        let unionId = this.getUnionId();
        let myLightOffTime = await this.service.light.myLightOffTime(unionId);
        this.success(myLightOffTime, 'my sleep records are as follow', 200);
    };


    //查找室友的关灯记录
    async roomieLightOffTime() {
        let unionId = this.getUnionId();
        let roomieUnionId = await this.service.match.getCurrentMatchUnionId(unionId);
        let roomieLightOffTime = await this.service.light.roomieLightOffTime(roomieUnionId);
        this.success(roomieLightOffTime, 'this the lightOffTime of your roomie', 200);
    }


    //从数据库读取昨晚关灯的用户数
    async lightOffTime() {
        let turnOffCount = await this.service.light.lightOffTime();
        this.success(turnOffCount, 'the count people which turn off their lights', 200);
    };

    //当前用户的灯的状态
    async lightStatus() {
        let unionId = this.getUnionId();
        let status = await this.service.light.lightStatus(unionId);
        this.success(status, 'this is the status of users light', 200);
    };
};

module.exports = LightController;