'use strict';
const BaseController = require('./base');

class MatchController extends BaseController {
    /**
     * 获得匹配门牌号
     */
    async acquireMatchCode() {
        const { service } = this;
        let unionId = this.getUnionId();
        let assignedCode = await service.match.acquireMatchCode(unionId);
        this.success({ assignedCode }, 'acquire match code success', 200);
    }

    /**
     * 获得匹配窗口
     */
    async getMatchWindow() {
        const { service } = this;
        let matchWindow = await service.match.getMatchWindow();
        this.success({ matchWindow }, 'get match window success', 200);
    }

    /**
     * 获得当前匹配者信息
     */
    async getCurrentMatch() {
        const { service } = this;
        let unionId = this.getUnionId();
        let currentMatch = await service.match.getCurrentMatch(unionId);
        this.success({ currentMatch }, 'get current match success', 200);
    }

    /**
     * 发送匹配请求
     */
    async sendMatchRequest() {
        const { ctx, app, service } = this;

        const bodyRule = {
            code: { type: 'string' },
        };
        ctx.validate(bodyRule, ctx.request.body);

        let senderId = this.getUnionId();
        let { sendRequestWrapper, receiverId } = await service.matchRequest.sendMatchRequest(senderId);
        let receiverSocketId = await app.redis.get(`socket@${receiverId}`);
        if (sendRequestWrapper && receiverSocketId) {
            receiverSocketId = receiverSocketId.replace('socket@', '');
            this.socketSuccess(receiverSocketId, app.config.socketChannel.sendMatchRequest, { sendRequestWrapper }, 'new match request');
        }
        this.success(undefined, 'send match request success', 200);
    }

    /**
     * 获取已收到的匹配请求
     */
    async getReceivedMatchRequest() {
        const { service } = this;
        let receiverId = this.getUnionId();
        let receivedRequests = await service.matchRequest.getReceivedMatchRequest(receiverId);
        this.success({ receivedRequests }, 'get received request success', 200);
    }

    /**
     * 回复匹配请求
     */
    async replyMatchRequest() {
        const { ctx, app, service } = this;

        const bodyRule = {
            requestId: { type: 'string' },
            replyStatus: { type: 'string' },
        };
        ctx.validate(bodyRule, ctx.request.body);

        let receiverId = this.getUnionId();
        let { replyRequestWrapper, senderId } = await service.matchRequest.replyPendingMatchRequet(receiverId);
        let senderSocketId = await app.redis.get(`socket@${senderId}`);

        if (replyRequestWrapper && senderSocketId) {
            senderSocketId = senderSocketId.replace('socket@', '');
            this.socketSuccess(senderSocketId, app.config.socketChannel.replyMatchRequest, { replyRequestWrapper }, 'new match request');
        }

        this.success(undefined, 'reply success', 200);
    }

    /**
     * 获取已发送的匹配请求
     */
    async getSentMatchRequest() {
        const { ctx, service } = this;
        const queryRule = {
            pageSize: { type: 'string' },
            pageNumber: { type: 'string' },
        };
        ctx.validate(queryRule, ctx.request.query);
        let unionId = this.getUnionId();
        let sentMatchRequest = await service.matchRequest.getSentMatchRequest(unionId);
        this.success({ sentMatchRequest }, 'sent match request', 200);
    }

    /**
     * 更新当前匹配
     */
    async renewCurrentMatch() {
        const { service } = this;
        let unionId = this.getUnionId();
        await service.match.renewCurrentMatch(unionId);
        this.success(undefined, 'renew success', 200);
    }

}

module.exports = MatchController;
