'use strict';
const BaseController = require('./base');

class ReportController extends BaseController {

    /**
     * 举报当前匹配
     */
    async reportCurrentMatch() {
        const { ctx, service } = this;
        const bodyRule = {
            reason: { type: 'string' },
            category: { type: 'string' }
        };
        ctx.validate(bodyRule, ctx.request.body);

        let unionId = this.getUnionId();
        await service.report.reportCurrentMatch(unionId);
        this.success(undefined, 'report success', 200);
    }

}

module.exports = ReportController;