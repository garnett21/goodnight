'use strict';
const BaseController = require('./base');

class SystemController extends BaseController {
    /**
     * 获得系统时间
     */
    async getSystemTime(){
        const {service} = this;
        let time = await service.system.getSystemTime();
        this.success({time},'get system time success',200);
    }
}

module.exports = SystemController;