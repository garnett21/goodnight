'use strict';
const BaseController = require('./base');

class UserController extends BaseController {

    /**
     * 微信小程序登录
     */
    async miniappLogin() {
        const { ctx, service } = this;
        const bodyRule = {
            code: { type: 'string' },
            iv: { type: 'string' },
            encryptedData: { type: 'string' },
        };
        const headerRule = {
            platform: { type: 'string' },
        };
        ctx.validate(headerRule, ctx.request.header);
        ctx.validate(bodyRule, ctx.request.body);
        let token = await service.user.getMiniappLoginToken();
        if (token !== undefined) {
            this.success({ token }, 'mini app login success', 200)
        }
        else {
            this.fail('mini app login failed', 400);
        }
    }

    /**
     * 移动平台登录(ios/android)
     */
    async mobileLogin() {
        const { ctx, service } = this;
        const bodyRule = {
            code: { type: 'string' },
        };
        const headerRule = {
            platform: { type: 'string' },
        };
        ctx.validate(headerRule, ctx.request.header);
        ctx.validate(bodyRule, ctx.request.body);
        let token = await service.user.getMobileLoginToken();
        if (token !== undefined) {
            this.success({ token }, 'mobile login success', 200)
        }
        else {
            this.fail('mobile login failed', 400);
        }
    }

    /**
     * 获取用户信息
     */
    async getUserInfo() {
        const { service } = this;
        let unionId = this.getUnionId();
        let userInfo = await service.user.getUserInfo(unionId);
        this.success({ userInfo }, 'get user info success', 200);
    }

    /**
     * VIP激活
     */
    async activateVIP() {
        const { ctx, service } = this;
        let unionId = this.getUnionId();
        const bodyRule = {
            code: { type: 'string' },
        };
        ctx.validate(bodyRule);
        await service.user.activateVIP(unionId);
        this.success(undefined, 'activate vip success', 200);
    }

    /**
     * 根据匹配码查找用户
     */
    async getUserInfoByMatchCode() {
        const { ctx, service } = this;
        const queryRule = {
            matchCode: { type: 'string' },
        }
        ctx.validate(queryRule, ctx.request.query);
        let userInfoWrapper = await service.user.getUserInfoByMatchCode();
        this.success(userInfoWrapper, 'get userinfo by match code success', 200);
    }

    /**
     * 更新昵称
     */
    async updateNickname() {
        const { ctx, service } = this;
        const bodyRule = {
            nickname: { type: 'string' },
        }
        let unionId = this.getUnionId();
        ctx.validate(bodyRule, ctx.request.body);
        await service.user.updateNickname(unionId);
        this.success(undefined, 'update nickname success', 200);
    }

    /**
     * 更新地区
     */
    async updateArea(){
        const { ctx, service } = this;
        const bodyRule = {
            area: { type: 'string' },
        }
        let unionId = this.getUnionId();
        ctx.validate(bodyRule, ctx.request.body);
        await service.user.updateArea(unionId);
        this.success(undefined, 'update area success', 200);
    }
}

module.exports = UserController;
