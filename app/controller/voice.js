'use strict';
const BaseController = require('./base');

class VoiceController extends BaseController {
    //每日推送的官方精选语音
    async officialVoicePush() {
        let officialVoicePush = await this.service.voice.officialVoicePush();
        this.success(officialVoicePush, 'this is the voice today', 200);
    };

    //查看往期的所有语音
    async reviewOldVoices() {
        let oldVoices = await this.service.voice.reviewOldVoices();
            this.success(oldVoices, 'these are the previous voices', 200);
    };

    //VIP的每日推送
    async vipVoicePush() {
        let unionId = this.getUnionId();
        let vipVoicePush = await this.service.voice.vipVoicePush(unionId);
        this.success(vipVoicePush, 'this is the vip push today', 200);
    };

    /**
     * 用户上传语音
     */
    async uploadAudio() {
        const { ctx, service } = this;
        const queryRule = {
            duration: { type: 'string' },
        };
        ctx.validate(queryRule, ctx.request.query);
        let unionId = this.getUnionId();
        await service.voice.uploadAudio(unionId);
        this.success(undefined, 'user audio upload success', 200);
    }
};
module.exports = VoiceController;