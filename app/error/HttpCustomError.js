
class HttpCustomError extends Error {
    constructor(errorInfo) {
        super(errorInfo)
        this.name = 'HttpCustomError';
        this.message = errorInfo.message;
        this.errorCode = errorInfo.errorCode;
        this.status = errorInfo.status;
        Error.captureStackTrace(this, this.constructor)
    }
}

module.exports = HttpCustomError;
