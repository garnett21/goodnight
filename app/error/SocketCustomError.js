
class SocketCustomError extends Error {
    constructor(socketId, channel, errorInfo) {
        super(errorInfo)
        this.name = 'SocketCustomError';
        this.message = errorInfo.message;
        this.errorCode = errorInfo.errorCode;
        this.socketId = socketId;
        this.channel = channel;
        Error.captureStackTrace(this, this.constructor)
    }
}

module.exports = SocketCustomError;
