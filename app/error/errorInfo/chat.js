/**
 * 语音聊天模块错误 03
 */
module.exports = {
    CURRENT_MATCH_NOT_EXIST: {
        errorCode: 1030001,
        status: 404,
        message: 'current match not exist'
    },

    OSS_UPLOAD_ERROR: {
        errorCode: 1030002,
        status: 405,
        message: 'oss upload error'
    },

    MINIAPP_MSG_COUNT_EXCEED_DAILY_LIMIT: {
        errorCode: 1030003,
        status: 403,
        message: 'mini app message count exceed daily limit'
    }

}