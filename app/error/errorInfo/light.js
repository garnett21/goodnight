/**
 * 灯模块错误 05
 */
module.exports = {
    NO_AVAILABLE_LIGHTS: {
        errorCode: 1050000,
        status: 400,
        message: 'no available lights'
    },

    CHANGE_FAIL: {
        errorCode: 1050001,
        status: 401,
        message: 'change fail'
    },

    NOT_CORRECT_TIME_FOR_TURNOFFLIGHT: {
        errorCode: 1050002,
        status: 402,
        message: 'it is not a correct time for turning off light'
    },

    CAN_NOT_TURN_OFF_AGAIN: {
        errorCode: 1050003,
        status: 403,
        message: 'you can not turn off your light again'
    },

    CAN_NOT_FIND_MY_RECORDS: {
        errorCode: 1050004,
        status: 404,
        message: 'can not find my records'
    },

    NO_ROOMIE: {
        errorCode: 1050005,
        status: 405,
        message: 'you have no roomie'
    },

    NOT_CORRECT_TIME_FOR_QUERYING: {
        errorCode: 1050006,
        status: 406,
        message: 'it is not a correct time for querying'
    },

    NO_EOOMIE_RECORDS: {
        errorCode: 1050007,
        status: 407,
        message: 'your roomie has no records'
    },
}