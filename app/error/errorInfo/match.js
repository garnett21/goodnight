/**
 * 匹配模块错误 02
 */
module.exports = {
    NOW_IS_NOT_IN_MATCH_WINDOW:{
        errorCode:1020001,
        status:400,
        message:'now is not in match window'
    },

    RANDOM_MATCH_ALREADY_EXIST:{
        errorCode:1020002,
        status:400,
        message:'match already exist'
    },

    RANDOM_EXIST_IN_MATCH_QUEUE:{
        errorCode:1020003,
        status:400,
        message:'match exist in match queue'
    },

    ASSIGN_MATCH_CODE_ERROR:{
        errorCode:1020004,
        status:400,
        message:'match code assign error'
    },

    REPLY_STATUS_CODE_ERROR:{
        errorCode:1020005,
        status:422,
        message:'reply status code error'
    },

    MATCH_REQUEST_NOT_FOUND:{
        errorCode:1020006,
        status:404,
        message:'match request not found'
    },

    RECEIVER_MATCH_ALREADY_EXIST:{
        errorCode:1020007,
        status:400,
        message:'receiver match already exist'
    },

    RECEIVER_EXIST_IN_MATCH_QUEUE:{
        errorCode:1020008,
        status:400,
        message:'receiver exist in match queue'
    },

    SENDER_EXIST_IN_MATCH_QUEUE:{
        errorCode:1020009,
        status:400,
        message:'sender exist in match queue'
    },

    SENDER_MATCH_ALREADY_EXIST:{
        errorCode:1020010,
        status:400,
        message:'sender match already exist'
    },

    LATEST_MATCH_REQUEST_WITHIN_MATCH_WINDOW:{
        errorCode:1020011,
        status:400,
        message:'latest match request within match window'
    },

    MATCH_CODE_NOT_FOUND:{
        errorCode:1020012,
        status:404,
        message:'match code not found'
    },

    CURRENT_MATCH_ALREADY_RENEWED:{
        errorCode:1020013,
        status:400,
        message:'current match already renewed'
    },

    REQUEST_COUNT_EXCEED_DAILY_LIMIT:{
        errorCode:1020014,
        status:400,
        message:'request count exceed daily limit'
    },

    MATCH_CODE_BELONGS_TO_SENDER:{
        errorCode:1020015,
        status:400,
        message:'match code belongs to sender'
    },

    USER_EXIST_IN_BANNED_LIST:{
        errorCode:1020016,
        status:403,
        message:'user exist in banned list'
    },

    REQUEST_SENDER_EXIST_IN_BANNED_LIST:{
        errorCode:1020017,
        status:403,
        message:'request sender exist in banned list'
    },

    REQUEST_RECEIVER_EXIST_IN_BANNED_LIST:{
        errorCode:1020018,
        status:403,
        message:'request receiver exist in banned list'
    }


};
