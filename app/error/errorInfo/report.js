/**
 * 举报模块错误 07
 */
module.exports = {
    CURRENT_MATCH_ALREADY_BEING_REPORTED: {
        errorCode: 1070001,
        status: 400,
        message: 'current match already being reported'
    },

    NO_MESSAGE_RECEIVED_YET: {
        errorCode: 1070002,
        status: 404,
        message: 'no message received yet'
    },

    CURRENT_MATCH_BEING_REPORTED_NOT_EXIST:{
        errorCode: 1070003,
        status: 404,
        message: 'current match being reported not exist'
    }
}