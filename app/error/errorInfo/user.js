/**
 * 用户模块错误 01
 */
module.exports = {
    USER_NOT_FOUND:{
        errorCode:1010001,
        status:404,
        message:'user not found'
    },

    ACTIVATION_ERROR:{
        errorCode:1010002,
        status:400,
        message:'activation error'
    },

    SEQUENCE_CODE_NOT_FOUND:{
        errorCode:1010003,
        status:404,
        message:'sequence code not found'
    },

    USER_ALREADY_ACTIVATED:{
        errorCode:1010004,
        status:403,
        message:'user already activated'
    },
}