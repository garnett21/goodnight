/**
 * 语音模块错误 04
 */
module.exports = {
    DAILY_AUDIO_COUNT_EXCEED_LIMIT:{
        errorCode:1040001,
        status:400,
        message:'daily upload count exceed limit'
    },

    AUDIO_UPLOAD_ERROR:{
        errorCode:1040002,
        status:401,
        message:'audio upload error'
    },

    NO_OLD_VIOICES:{
        errorCode:1040003,
        status:402,
        message:'there is no old voices'
    },

    CAN_NO_FIND_ROOMID:{
        errorCode:1040004,
        status:403,
        message:'can no find author roomId'
    },
    
}