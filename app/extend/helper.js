module.exports = {
    decodeToken(token) {
        const { app } = this;
        token = token.split(' ')[1];
        let decoded = app.jwt.decode(
            token,
            { complete: true }
        );
        return decoded.payload;
    },

    decodeTokenFromSocket(token) {
        const { app } = this;
        let decoded = app.jwt.decode(
            token,
            { complete: true }
        );
        return decoded.payload;
    },

    dateFormater(dateObj) {
        return `${dateObj.getFullYear()}-${dateObj.getMonth() + 1}-${dateObj.getDate()}`;
    },

    isNowWithinMatchWindow(matchWindow) {
        let now = new Date();
        let periodBeginTime = new Date(matchWindow.beginTime);
        let periodEndTime = new Date(matchWindow.endTime);

        if (now < periodBeginTime || now > periodEndTime) {
            return false;
        }
        else {
            return true;
        }
    },

    isTimestampWithinMatchWindow(timestamp, matchWindow) {
        timestamp = new Date(timestamp);
        let periodBeginTime = new Date(matchWindow.beginTime);
        let periodEndTime = new Date(matchWindow.endTime);
        if (timestamp < periodBeginTime || timestamp > periodEndTime) {
            return false;
        }
        else {
            return true;
        }
    },


    getYesterday() {
        const dayMilliseconds = 86400000;
        let yesterday = new Date(new Date().getTime() - dayMilliseconds);
        return yesterday;
    },

    getTomorrow() {
        const dayMilliseconds = 86400000;
        let tomorrow = new Date(new Date().getTime() + dayMilliseconds);
        return tomorrow;
    },

    getToday() {
        let today = new Date(new Date().getTime());
        return today;
    }

};
