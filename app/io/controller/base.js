'use strict';
const Controller = require('egg').Controller;

class BaseController extends Controller {
    socketSuccess(socketId, channel, data, message) {
        const {app} = this;
        let wrappedData = {
            success: true,
            data: data,
            message: message,
        }
        app.io.to(socketId).emit(channel, wrappedData);
    }

    socketFail(socketId, channel, message) {
        const {app} = this;
        let wrappedData = {
            success: false,
            message: message,
        }
        app.io.to(socketId).emit(channel, wrappedData);
    }

    getUnionIdFromSocket() {
        let payload = this.ctx.helper.decodeTokenFromSocket(this.ctx.socket.handshake.query.token);
        return payload.unionId;
    }

    getPlatformFromSocket() {
        let payload = this.ctx.helper.decodeTokenFromSocket(this.ctx.socket.handshake.query.token);
        return payload.platform;
    }
}

module.exports = BaseController;
