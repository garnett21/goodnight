const BaseController = require('./base');

class MatchController extends BaseController {
    async processMatch() {
        const { ctx, service, app } = this;
        let unionId = this.getUnionIdFromSocket();
        let socketId = ctx.socket.id;
        let allowTomatch = await service.match.isAllowToMatch(unionId,socketId);
        if (allowTomatch) {
            if (await service.match.enqueue(unionId, socketId)) {
                let matches = await service.match.dequeueFront();
                if (matches) {
                    for (let i = 0; i < matches.length; i++) {
                        let self = matches[i];
                        let match = i === 0 ? matches[i + 1] : matches[i - 1];
                        this.socketSuccess(self.socketId, app.config.socketChannel.randomMatch, { match }, 'match success');
                    }
                }
            }
        }
    }
}

module.exports = MatchController;
