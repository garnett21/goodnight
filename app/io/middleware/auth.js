'use strict';

module.exports = () => {
  return async (ctx, next) => {
    try {
      let token = ctx.socket.handshake.query.token;
      let socketId = ctx.socket.id;
      let payload = ctx.app.jwt.verify(token, ctx.app.config.jwt.secret);
      //保存socketId进redis
      ctx.app.redis.set(`socket@${payload.unionId}`, socketId, `ex`, 7200);
      return await next();

    } catch (error) {
      console.error(error);
      ctx.socket.disconnect();
      return;
    }
  };
};
