module.exports = app => {
    return async (ctx, next) => {
        try {
            await next();
        } catch (err) {
            // 所有的异常都在 app 上触发一个 error 事件，框架会记录一条错误日志
            ctx.app.emit('error', err, ctx);

            const message  = err.message;
            const errorCode = err.errorCode;
            const channel = err.channel;
            const socketId = err.socketId;

            let errorData = {
                success: false,
                message: message,
                errorCode: errorCode
            }
            app.io.to(socketId).emit(channel, errorData);

        }
    };
  };