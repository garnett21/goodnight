const vipErrorInfo = require('../error/errorInfo/vip');
const HttpCustomError = require('../error/HttpCustomError');
module.exports = options => {
    return async function vipJudgement(ctx, next) { 
        let payload = ctx.helper.decodeToken(ctx.request.header.authorization);
        let unionId = payload.unionId;
        let userinfo = await ctx.model.Userinfo.getUserByUnionId(unionId);
        let vipExpire = userinfo.vip_expire;
        let now = new Date();
        let expireDay = new Date(vipExpire);
        let currentTime = now.getTime();
        let expireTime = expireDay.getTime();
        //let currentDate = ctx.helper.dateFormater(now);
        if(vipExpire == null){          
            throw new HttpCustomError(vipErrorInfo.NOT_VIP);
        }else{
            if(currentTime <= expireTime){
                return await next();
            }else{
                throw new HttpCustomError(vipErrorInfo.NOT_VIP);     
            };
        };
        
    };
};