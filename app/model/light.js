module.exports = app => {
    const { STRING, INTEGER } = app.Sequelize;
    //light表模型
    const Light = app.model.define('light', {
        lightId: {
            type: INTEGER,
            primaryKey: true
        },
        title: STRING,
        sceneLighton_url: STRING,
        sceneLightoff_url: STRING,
        vip_light: STRING,
        status: STRING,
        thumbnailLighton_url: STRING,
        thumbnailLightoff_url: STRING,
        lthumbnailUnusable_url: STRING
    }
        , { timestamps: false }
    );

    //查询目前使用灯的皮肤url
    Light.picurlOfCurrentLight = async function (lightId) {

        let picurlOfCurrentLight = await Light.findOne({
            raw: true,
            attributes: ['title', 'vip_light', 'sceneLighton_url', 'sceneLightoff_url', 'thumbnailLighton_url', 'thumbnailLightoff_url', 'thumbnailUnusable_url'],
            where: {
                lightId: lightId
            }
        });
        return picurlOfCurrentLight;


    };

    //展示目前可用的所有灯的信息
    Light.showAvailableLight = async function () {
        let showAvailableLight = await Light.findAll({
            raw: true,
            attributes: ['lightId', 'title', 'vip_light', 'sceneLighton_url', 'sceneLightoff_url', 'thumbnailLighton_url', 'thumbnailLightoff_url', 'thumbnailUnusable_url'],
            where: {
                status: '1'
            }
        });
        return showAvailableLight;
    };

    //展示目前可用的非VIP用户可用的灯的ID
    Light.showAvailableNotVipLightId = async function () {    
        let showAvailableNotVipLightId = await Light.findAll({
            raw: true,
            attributes: ['lightId'],
            where: {
                vip_light:'0',
                status: '1'
            }
        });
        return showAvailableNotVipLightId;
    };

    //展示目前可用的VIP用户的灯的ID
    Light.showAvailableLightId = async function () {

        let showAvailableLightId = await Light.findAll({
            raw: true,
            attributes: ['lightId'],
            where: {
                status: '1'
            }
        });
        return showAvailableLightId;


    };
    return Light;

};