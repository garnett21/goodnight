module.exports = app => {
    const { STRING, TIME } = app.Sequelize;

    const MatchBannedList = app.model.define('match_banned_list', {
        unionId: {
            type: STRING,
            primaryKey: true
        },
        expireAt: TIME,
        createdAt: TIME,
        updatedAt: TIME,
    },
        {
            freezeTableName: true,
            timestamps: true,
            underscored: false,
        });


    MatchBannedList.isUserExistInBannedList = async function (unionId) {
        let now = new Date();
        let count = await this.count({
            where: {
                unionId: unionId,
                expireAt: {
                    [app.Sequelize.Op.gte]: now
                }
            }
        });

        if (count === 1) {
            return true;
        }
        return false;
    }

    return MatchBannedList;
};