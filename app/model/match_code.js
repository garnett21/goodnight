module.exports = app => {
    const { INTEGER, STRING, TIME } = app.Sequelize;
    const ACTIVATE_STATUS = 1;
    const NOT_ACTIVATE_STATUS = 0;

    const MatchCode = app.model.define('match_code', {
        code: {
            type: STRING,
            primaryKey: true
        },
        status: INTEGER,
        updatedAt: TIME
    },
        {
            freezeTableName: true,
            timestamps: false,
            underscored: false
        });

    MatchCode.assignMatchCode = async function () {
        try {
            let assignedCode = await app.model.transaction(async (trans) => {
                let codeRecord = await this.findOne({
                    attributes: ['code'],
                    raw: true,
                    transaction: trans,
                    order: [
                        app.Sequelize.fn('RAND'),
                    ],
                    lock: trans.LOCK.UPDATE,
                    where: {
                        status: NOT_ACTIVATE_STATUS
                    }
                });

                //查找失败,抛出异常回滚事务
                if (codeRecord === null) {
                    throw new Error('Rollback initiated');
                }
                let updateResult = await this.update(
                    { status: ACTIVATE_STATUS, },
                    {
                        transaction: trans,
                        where:
                        {
                            code: codeRecord.code
                        }
                    });
                if (updateResult[0] === 1) {
                    return codeRecord.code;
                }
                return null;
            });
            return assignedCode;
        } catch (err) {
            console.log(err);
            return null;
        }
    }

    return MatchCode;
};