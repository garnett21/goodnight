module.exports = app => {
    const { STRING, INTEGER, TIME } = app.Sequelize;
    const NOT_RENEW_STATUS = 0;
    const RENEW_STATUS = 1;

    const MatchCurrent = app.model.define('match_current', {
        unionId: {
            type: STRING,
            primaryKey: true
        },
        matchId: STRING,
        renew: INTEGER,
        updatedAt: TIME,
    },
        {
            freezeTableName: true,
            timestamps: true,
            createdAt: false,
            underscored: false
        });


    MatchCurrent.getCurrentMatchByUnionId = async function (unionId) {
        return await this.findOne({
            raw: true,
            where: {
                unionId: unionId
            }
        });
    }

    MatchCurrent.createCurrentMatch = async function (unionId, matchId) {
        return await this.create({
            unionId: unionId,
            matchId: matchId,
            renew: NOT_RENEW_STATUS,
        });
    }

    MatchCurrent.updateCurrentMatch = async function (unionId, matchId, renewal) {
        return await this.update(
            {
                matchId: matchId,
                renew: renewal,
            },
            {
                where: {
                    unionId: unionId
                }
            });
    }

    MatchCurrent.isCurrentMatchExist = async function (unionId) {
        let count = await this.count({
            where: {
                unionId: unionId
            }
        });
        if (count == 1) {
            return true;
        }
        return false;
    }

    MatchCurrent.setRenewStatus = async function (unionId) {
        return await this.update({
            renew: RENEW_STATUS
        },
            {
                where: {
                    unionId: unionId,
                    renew: NOT_RENEW_STATUS
                }
            });
    }

    MatchCurrent.renewCurrentMatch = async function (renewList, updatedAt) {
        return await this.update({
            renew: NOT_RENEW_STATUS,
            updatedAt: updatedAt
        },
            {
                where: {
                    unionId: {
                        [app.Sequelize.Op.in]: renewList
                    },
                },
                silent: true 
            });
    }


    MatchCurrent.getAllRecordWillingToRenew = async function () {
        return await this.findAll({
            attributes: ['unionId', 'matchId'],
            raw: true,
            where: {
                renew: RENEW_STATUS
            }
        });
    }

    return MatchCurrent;
};