module.exports = app => {
    const { STRING, TIME } = app.Sequelize;

    const MatchHistory = app.model.define('match_history', {
        unionId: {
            type: STRING,
            primaryKey: true
        },
        matchId: STRING,
        createdAt: {
            type: TIME,
            primaryKey: true
        }
    },
        {
            freezeTableName: true,
            timestamps: false,
            underscored: false
        });

    MatchHistory.createMatchHistoryRecord = async function (unionId, matchId) {
        return await this.create({
            unionId: unionId,
            matchId: matchId
        });
    }

    MatchHistory.createMatchHistoryRecordViaRenew = async function (history) {
        return await this.bulkCreate(history,
            {
                silent: true
            });
    }

    MatchHistory.getMatchHistoryForVoiceMessage = async function (unionId) {
        return await this.findAll({
            raw: true,
            where: {
                unionId: unionId,
            },
            limit: 7,
            order: [
                ['createdAt', 'desc']
            ],
        });
    }
    return MatchHistory;
};