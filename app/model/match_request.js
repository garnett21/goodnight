module.exports = app => {
    const { STRING, INTEGER, TIME } = app.Sequelize;
    const uuidv4 = require('uuid/v4');
    const PENDING_STATUS = 0;
    const DEACTIVATED_STATUS = -2;

    const MatchRequest = app.model.define('match_request', {
        requestId: {
            type: STRING,
            primaryKey: true
        },
        senderId: STRING,
        receiverId: STRING,
        status: INTEGER,
        expireAt: TIME
    },
        {
            freezeTableName: true,
            timestamps: true,
            underscored: false
        });

    MatchRequest.createMatchRequest = async function (senderId, receiverId, expireAt) {
        let requestId = uuidv4();
        await this.create({
            requestId: requestId,
            senderId: senderId,
            receiverId: receiverId,
            status: PENDING_STATUS,
            expireAt: expireAt
        });
        return requestId;
    }

    MatchRequest.getLatestRequestBySenderIdAndReceiverId = async function (senderId, receiverId) {
        return await this.findOne({
            where: {
                senderId: senderId,
                receiverId: receiverId
            },
            order: [
                ['createdAt', 'desc']
            ]
        })
    }

    MatchRequest.getReceivedReqeustBetweenMatchWindow = async function (receiverId, matchWindow) {
        return await this.findAll({
            raw: true,
            where: {
                receiverId: receiverId,
                createdAt: {
                    [app.Sequelize.Op.between]: [matchWindow.beginTime, matchWindow.endTime],
                }
            }
        })
    }

    MatchRequest.getSentRequestCountWithinCurrentMatchWindow = async function (senderId, matchWindow) {
        return await this.count({
            where: {
                senderId: senderId,
                createdAt: {
                    [app.Sequelize.Op.between]: [matchWindow.beginTime, matchWindow.endTime],
                }
            }
        });
    }

    MatchRequest.getMatchRequestByRequestId = async function (requestId) {
        return await this.findOne({
            raw: true,
            where: {
                requestId: requestId,
            }
        });
    }

    MatchRequest.updateReceivedPendingRequest = async function (requestId, replyStatus, now) {
        return await this.update({
            status: replyStatus
        },
            {
                where: {
                    requestId: requestId,
                    expireAt: {
                        [app.Sequelize.Op.gt]: now
                    }
                }
            })
    }

    MatchRequest.deactivateAllPendingMatchRequestByUnionId = async function (unionId) {
        await this.update(
            {
                status: DEACTIVATED_STATUS
            },
            {
                where: {
                    [app.Sequelize.Op.or]: [{ senderId: unionId }, { receiverId: unionId }],
                    status: PENDING_STATUS
                }
            })
    }

    MatchRequest.getSentMatchRequestBetweenMatchWindow = async function (unionId, pageNumber, pageSize, matchWindow) {
        return await this.findAll({
            where: {
                senderId: unionId,
                createdAt: {
                    [app.Sequelize.Op.between]: [matchWindow.beginTime, matchWindow.endTime],
                }
            },
            limit: pageSize,
            offset: (pageNumber - 1) * pageSize,
            order: [
                ['createdAt', 'desc']
            ]
        })
    }

    MatchRequest.deactivateAllPendingMatchRequest = async function () {
        return await this.update(
            {
                status: DEACTIVATED_STATUS
            },
            {
                where: {
                    status: PENDING_STATUS
                }

            });
    }

    return MatchRequest;
};