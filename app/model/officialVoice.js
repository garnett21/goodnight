module.exports = app => {
    const { STRING, DATE, INTEGER } = app.Sequelize;

    const OfficialVoice = app.model.define('official_voice', {
        audioId: {
            type: INTEGER,
            primaryKey: true
        },
        pushTime: DATE,
        duration: INTEGER,
        format: STRING,
        authorNickname: STRING
    }, {
            freezeTableName: true,
            timestamps: false
        }
    );

    //查看官方每日推送的语音
    OfficialVoice.officialVoicePush = async function (currentDate) {
        return await this.findOne({
            raw: true,
            attributes: ['audioId','duration','pushTime','authorNickname','format'],
            where: {
                pushTime: currentDate
            }
        });
    };


    //查看往期的语音
    OfficialVoice.reviewOldVoices = async function (currentDate) {
        return await this.findAll({
            raw: true,
            attributes: ['audioId','duration','pushTime','authorNickname','format'],
            where: {
                pushTime: {
                    lt: currentDate
                }
            }
        });
    };


    //查询语音格式
    OfficialVoice.formatJudgement = async function(audioId){
      return await this.findOne({
          raw:true,
          attributes:['format'],
          where: {
              audioId:audioId
          }
      })
    };

   //查询作者昵称
   OfficialVoice.findAuthorNickname = async function(audioId){
       return await this.findOne({
           raw:true,
           attributes:['authorNickname'],
           where: {
            audioId:audioId
           }
       })
   };





    return OfficialVoice;
};