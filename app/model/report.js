module.exports = app => {
    const { STRING, TIME, INTEGER } = app.Sequelize;
    const REPORT_PENDING_STATUS = 0;
    const Report = app.model.define('report', {
        informantId: {
            type: STRING,
            primaryKey: true
        },
        suspectId: STRING,
        reason: STRING,
        category: INTEGER,
        status: INTEGER,
        createdAt: {
            type: TIME,
            primaryKey: true
        },
        updatedAt: TIME,
    },
        {
            freezeTableName: true,
            timestamps: true,
            underscored: false,
        });


    Report.createReport = async function (informantId, suspectId, reason, category) {
        return await this.create({
            informantId: informantId,
            suspectId: suspectId,
            reason: reason,
            category: category,
            status: REPORT_PENDING_STATUS,
        });
    }

    Report.isReportExistWithinWindow = async function (informantId, window) {
        let count = await this.count({
            where: {
                informantId: informantId,
                createdAt: {
                    [app.Sequelize.Op.between]: [window.beginTime, window.endTime],
                }
            }
        });
        if (count > 0) {
            return true;
        }
        return false;
    }

    return Report;
};