module.exports = app => {
    const { INTEGER, STRING, TIME } = app.Sequelize;
    const ACTIVATE_STATUS = 1;
    const NOT_ACTIVATE_STATUS = 0;

    const SequenceCode = app.model.define('sequence_code', {
        id: {
            type: INTEGER,
            primaryKey: true
        },
        code: STRING,
        status: INTEGER,
        code_number: INTEGER,
        vip_expire: TIME,
        unionId: STRING,
        activatedAt: TIME

    },
        {
            freezeTableName: true,
            timestamps: false,
            underscored: false
        });


    SequenceCode.assignSequenceCode = async function (code, unionId) {
        return await this.update(
            {
                unionId: unionId,
                status: ACTIVATE_STATUS
            }, {
                raw: true,
                where: {
                    code: code
                }
            })
    }

    SequenceCode.getNotActivatedSequenceCodeByCode = async function (code) {
        return await this.findOne({
            raw: true,
            where: {
                code: code,
                status: NOT_ACTIVATE_STATUS
            }
        });
    }

    return SequenceCode;
};