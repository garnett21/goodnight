module.exports = app => {
    const { STRING, TIME } = app.Sequelize;

    const Turn = app.model.define('light_off_time', {
        unionId: {
            type: STRING,
            primaryKey: true
        },
        turnOffTime: TIME
    }
        , {
            freezeTableName: true,
            timestamps: false
        }
    );

    //把关灯时间写入数据库
    Turn.turnOffLight = async function (unionId, time) {
        return await this.create({
            unionId: unionId,
            turnOffTime: time
        });
    }

    //查询用户今晚有没有关过灯
    Turn.findRecords = async function (unionId,beginTime,endTime) {
        return await this.count({
            raw:true,
            where:{
                turnOffTime: {
                    $between: [beginTime, endTime]   
                },
                unionId:unionId
        }
    })
}
    //查看我的睡眠记录
    Turn.myLightOffTime = async function (unionId) {
        return await this.findAll({
            raw: true,
            where: {
                unionId: unionId
            }
        });
    };
    
    //查询今晚关灯的人数
    Turn.countRecords = async function (beginTime, endTime) {
        let countRecords = await this.count({
            raw: true,
            where: {
                turnOffTime: {
                    $between: [beginTime, endTime]
                }
            }
        });
        //console.log(countRecords);
        return countRecords;

    };
    return Turn;
};