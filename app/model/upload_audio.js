module.exports = app => {
    const { STRING, TIME, INTEGER } = app.Sequelize;

    const UploadAudio = app.model.define('upload_audio', {
        audioId: {
            type: STRING,
            primaryKey: true
        },
        unionId: STRING,
        duration: INTEGER,
        createdAt: TIME,
    },
        {
            freezeTableName: true,
            timestamps: true,
            underscored: false,
            updatedAt: false,

        });


    UploadAudio.createUploadAudio = async function (audio) {
        return await this.create({
            audioId: audio.audioId,
            unionId: audio.unionId,
            duration: audio.duration,
        });
    }

    UploadAudio.getAudioDailyUploadCount = async function (unionId, dayWindow) {
        return await this.count({
            where: {
                unionId: unionId,
                createdAt: {
                    [app.Sequelize.Op.between]: [dayWindow.beginTime, dayWindow.endTime],
                }
            }
        })
    }

    UploadAudio.getAuthorUnionIdAndDurationByAudioId = async function(audioId){
      return await this.findOne({
          raw:true,
          attributes:['unionId','duration'],
          where :{
              audioId:audioId
          }
      })
    }






    return UploadAudio;
};