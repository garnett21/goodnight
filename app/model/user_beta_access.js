module.exports = app => {
    const { STRING, TIME } = app.Sequelize;

    const UserBetaAccess = app.model.define('user_beta_access', {
        unionId: {
            type: STRING,
            primaryKey: true
        },
        expireAt: TIME,
    },
        {
            freezeTableName: true,
            timestamps: false,
            underscored: false
        });

    UserBetaAccess.createAccessRecord = async function (unionId, expireAt) {
        return await this.create({
            unionId: unionId,
            expireAt, expireAt
        });
    }

    UserBetaAccess.getAccessRecordByUnionId = async function(unionId) {
        return await this.findOne({
            raw:true,
            where:{
                unionId:unionId
            }
        });
    }

    UserBetaAccess.isAccessRecordExist = async function(unionId) {
        let count =  await this.count({
            where:{
                unionId:unionId
            }
        });

        if(count===1){
            return true;
        }
        return false;
    }

    UserBetaAccess.updateAccessRecord = async function (unionId, expireAt) {
        return await this.update(
            {
                expireAt, expireAt
            },
            {
                where: {
                    unionId: unionId
                }
            });
    }

    return UserBetaAccess;
};