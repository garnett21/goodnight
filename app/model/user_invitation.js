module.exports = app => {
    const { STRING, INTEGER, TIME } = app.Sequelize;
    const NOT_COMPLETED_STATUS = 0;
    const COMPLETED_STATUS = 1;

    const UserInvitation = app.model.define('user_invitation', {
        unionId:
        {
            type: STRING,
            primaryKey: true
        },
        invitationId: {
            type: STRING,
            primaryKey: true
        },
        completed: INTEGER,
        expireAt: TIME,
        createdAt: TIME,
    },
        {
            freezeTableName: true,
            timestamps: true,
            underscored: false,
            updatedAt: false,
        });


    UserInvitation.getLatestInvitationByUnionId = async function (unionId) {
        return await this.findOne({
            raw: true,
            where: {
                unionId: unionId
            },
            order: [
                ['createdAt', 'desc']
            ]
        });
    }

    UserInvitation.getInvitationByInvitationId = async function (invitationId) {
        return await this.findOne({
            raw: true,
            where: {
                invitationId: invitationId
            }
        });
    }


    UserInvitation.createInvitation = async function (unionId, invitationId, expireAt) {
        return await this.create({
            unionId: unionId,
            invitationId: invitationId,
            expireAt: expireAt,
            completed: NOT_COMPLETED_STATUS,
        });
    }

    UserInvitation.updateInvitationCompleteStatus = async function (invitationId) {
        return await this.update(
            {
                completed: COMPLETED_STATUS,
            },
            {
                where: {
                    invitationId: invitationId
                }
            });
    }



    return UserInvitation;
}
