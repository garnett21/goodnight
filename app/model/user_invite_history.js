module.exports = app => {
    const { STRING, TIME } = app.Sequelize;

    const UserInviteHistory = app.model.define('user_invite_history', {
        invitationId: {
            type: STRING,
            primaryKey: true
        },
        inviteeId: {
            type: STRING,
            primaryKey: true
        },
        createdAt: TIME,
    },
        {
            freezeTableName: true,
            timestamps: true,
            underscored: false,
            updatedAt: false,
        });

    UserInviteHistory.createInvitationRecord = async function (invitationId, inviteeId) {
        return await this.create({
            invitationId: invitationId,
            inviteeId: inviteeId,
        });
    }

    UserInviteHistory.getIssuedInvitationRecordCount = async function (invitationId) {
        return await this.count({
            where: {
                invitationId: invitationId,
            }
        });
    }

    UserInviteHistory.getOldestThreeInvitationRecordByInvitationId = async function (invitationId) {
        return await this.findAll({
            raw: true,
            where: {
                invitationId: invitationId,
            },
            limit: 3,
            order: [
                ['createdAt', 'asc']
            ]
        })
    }

    UserInviteHistory.isInvitationExist = async function (invitationId, inviteeId) {
        let count = await this.count({
            where: {
                invitationId: invitationId,
                inviteeId: inviteeId,
            }
        });

        if (count > 0) {
            return true;
        }
        return false;
    }


    return UserInviteHistory;
};