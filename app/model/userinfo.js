module.exports = app => {
    const { STRING, TIME, INTEGER } = app.Sequelize;

    const Userinfo = app.model.define('userinfo', {
        unionId: {
            type: STRING,
            primaryKey: true
        },
        nickname: STRING,
        gender: INTEGER,
        vip_expire: TIME,
        area: STRING,
        avatar: STRING,
        match_code: STRING
    },
        {
            freezeTableName: true,
            timestamps: true,
            underscored: false
        });


    Userinfo.createUser = async function (data) {
        return await this.create({
            unionId: data.unionId,
            nickname: data.nickName,
            gender: data.gender,
            area: data.city,
            avatar: data.avatarUrl
        })
    }

    Userinfo.getUserByUnionId = async function (unionId) {
        return await this.findOne({
            raw: true,
            where: {
                unionId: unionId
            }
        });
    }

    
    Userinfo.getUserByMatchCode = async function (code) {
        return await this.findOne({
            raw: true,
            where: {
                match_code: code
            }
        });
    }

    Userinfo.isUserExist = async function (unionId) {
        let count = await this.count({
            where: {
                unionId: unionId
            }
        });
        if (count == 1) {
            return true;
        }
        return false;
    }

    Userinfo.updateVipExpireTime = async function (unionId, vip_expire) {
        return await this.update(
            {
                vip_expire: vip_expire
            },
            {
                where: {
                    unionId: unionId
                }
            })
    }

    Userinfo.isMatchCodeExist = async function (unionId) {
        let codeRecord = await this.findOne({
            attributes: ['match_code'],
            raw: true,
            where: {
                unionId: unionId
            }
        });
        if (codeRecord.match_code !== null) {
            return true;
        }
        return false;
    }

    Userinfo.updateMatchCode = async function (unionId, match_code) {
        return await this.update({
            match_code: match_code
        },
            {
                where: {
                    unionId: unionId
                }
            });
    }

    Userinfo.updateNickname = async function (unionId, nickname) {
        return await this.update({
            nickname: nickname
        }, {
                where: {
                    unionId: unionId
                }
            });
    }

    Userinfo.updateArea = async function (unionId, area) {
        return await this.update({
            area: area
        }, {
                where: {
                    unionId: unionId
                }
            });
    }

    return Userinfo;
};