module.exports = app => {
    const { STRING, INTEGER } = app.Sequelize;
    //user_light表模型
    const Userlight = app.model.define('user_light', {
        unionId: {
            type: STRING,
            primaryKey: true
        },
        lightId: INTEGER,
        status: INTEGER
    }
        , {
            freezeTableName: true,
            timestamps: false
        }
    );
    //查看当前选择的灯
    Userlight.showCurrentLight = async function (unionId) {
        return await this.findOne({
            raw: true,
            attributes: ['lightId', 'status'],
            where: {
                unionId: unionId
            }
        });
    };

    //把新用户选的灯写入数据库
    Userlight.newLight = async function (unionId, lightId,status) {
        return await this.create({
            unionId: unionId,
            lightId: lightId,
            status: status
        });

    };



    //是否存在记录
    Userlight.isRecordExist = async function (unionId) {
        return await this.findOne({
            raw: true,
            attributes: ['unionId'],
            where: {
                unionId: unionId
            }
        });
    };


    //换灯
    Userlight.changeLight = async function (unionId, lightId) {
        let pram = { 'unionId': unionId, 'lightId': lightId };
        return await this.update(
            pram, {
                raw: true,
                where: { unionId: unionId }
            }
        )
    };

    //每天自动开灯
    Userlight.turnOnLight = async function () {
        let pram = { 'status': 1 };
        return await this.update(
            pram, {
                raw: true,
                where: { status: 0 }
            }
        )

    };

    //关灯（插入）
    Userlight.turnOffNewLight = async function (unionId, lightId,status) {
        return await this.create({
            unionId: unionId,
            lightId: lightId,
            status: status
        });
    };


    //关灯（更新）
    Userlight.turnOffLight = async function (unionId) {
        let pram = { 'status': 0 };
        return await this.update(
            pram, {
                raw: true,
                where: { unionId: unionId }

            })
    };

    //当前用户的灯的状态
    Userlight.lightStatus = async function (unionId) {
        return await this.findOne({
            raw: true,
            attributes: ['status'],
            where: { unionId: unionId }
        });
    };
    return Userlight;
};