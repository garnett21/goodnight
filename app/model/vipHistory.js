module.exports = app =>{
    const { STRING } = app.Sequelize;

    const VipHistory = app.model.define('vip_push_history',{
        unionId: {
            type: STRING,
            primaryKey: true
        },
        voiceId: {
            type: STRING,
            primaryKey: true
        },        
        date: {
            type: STRING,
            primaryKey: true
        }
    }, {
        freezeTableName: true,
        timestamps: false
    }
    );

    //查找VIP的所有推送历史
    VipHistory.vipRecords = async function(unionId){
        return await this.findAll({
            raw:true,
            attributes: ['voiceId'],
            where: {
                unionId: unionId
            }
        });
    };

    //把VIP的推送记录写入数据库
    VipHistory.record = async function (unionId,voiceId,today) {
        return await this.create({
            unionId:unionId,
            voiceId:voiceId,
            date:today
        });
    };


    //通过日期查找推送记录
    VipHistory.findRecord = async function (unionId,today) {
        return await this.findOne({
            raw:true,
            attributes:['voiceId'],
            where:{
                unionId: unionId,
                date:today
            }
        });
    };

    return VipHistory;
};