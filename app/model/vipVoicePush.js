module.exports = app => {
    const { STRING, INTEGER } = app.Sequelize;
    const Op = app.Sequelize.Op


    const VipVoicePush = app.model.define('vip_voice', {
        voiceId: {
            type: INTEGER,
            primaryKey: true
        },
        audioId: STRING,
        duration: INTEGER,
        format: STRING,
        authorNickname: STRING
    }, {
            freezeTableName: true,
            timestamps: false
        });

    //VIP的每日推送(有推送记录)
    VipVoicePush.vipPushWithHistory = async function (history) {
        return await this.findOne({
            raw: true,
            attributes: ['voiceId','audioId', 'authorNickname','duration','format'],
            order: [
                app.Sequelize.fn('RAND'),
            ],
            where: {
                voiceId: {
                    [Op.notIn]: [history]
                }
            }

        })
    };


    //VIP的每日推送(无推送记录)
    VipVoicePush.vipPushWithNoHistory = async function () {
        return await this.findOne({
            raw: true,
            attributes: ['voiceId','audioId','authorNickname','duration','format'],
            order: [
                app.Sequelize.fn('RAND'),
            ]
        })
    };


    //查找一条语音
    VipVoicePush.findAVoice = async function (voiceId) {
        return await this.findOne({
            raw: true,
            attributes: ['voiceId','audioId','authorNickname','duration','format'],
            where:{
                voiceId:voiceId
            }
        });
    }

    //查询语音格式
    VipVoicePush.formatJudgement = async function(audioId){
        return await this.findOne({
            raw:true,
            attributes:['format'],
            where: {
                audioId:audioId
            }
        })
      };
  
     //查询作者昵称
     VipVoicePush.findAuthorNickname = async function(audioId){
         return await this.findOne({
             raw:true,
             attributes:['authorNickname'],
             where: {
              audioId:audioId
             }
         })
     };
  



    return VipVoicePush;
};