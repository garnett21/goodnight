module.exports = app => {
    const { STRING, TIME, INTEGER } = app.Sequelize;
    const NORMAL_MSG_STATUS = 0;
    const REPORTED_MSG_STAUS = 1;

    const VoiceMessage = app.model.define('voice_message', {
        messageId: {
            type: STRING,
            primaryKey: true
        },
        senderId: STRING,
        receiverId: STRING,
        duration: INTEGER,
        reportTag: INTEGER,
        createdAt: TIME,
    },
        {
            freezeTableName: true,
            timestamps: true,
            underscored: false,
            updatedAt: false,

        });


    VoiceMessage.createVoiceMessage = async function (message) {
        return await this.create({
            messageId: message.messageId,
            senderId: message.senderId,
            receiverId: message.receiverId,
            duration:message.duration,
            reportTag: NORMAL_MSG_STATUS
        });
    }


    VoiceMessage.updateVoiceMessageAsBeingReported = async function (reportedMessages) {
        return await this.update({
            reportTag:REPORTED_MSG_STAUS,
        },{
            where: {
                messageId: {
                    [app.Sequelize.Op.in]: reportedMessages
                },
            },
        });
    }

    VoiceMessage.getSentVoiceMessageWithinWindow = async function (senderId, window) {
        return await this.findAll({
            raw:true,
            where: {
                senderId: senderId,
                createdAt: {
                    [app.Sequelize.Op.between]: [window.beginTime, window.endTime],
                }
            }
        });
    }

    VoiceMessage.getReceivedVoiceMessageWithinWindow = async function (receiverId, window) {
        return await this.findAll({
            raw:true,
            where: {
                receiverId: receiverId,
                createdAt: {
                    [app.Sequelize.Op.between]: [window.beginTime, window.endTime],
                }
            }
        });
    }



    return VoiceMessage;
};