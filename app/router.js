'use strict';

/**
 * @param {Egg.Application} app - egg application
 */

 module.exports = app => {
  require('./router/light')(app);
  require('./router/voice')(app);
  require('./router/user')(app);
  require('./router/match')(app);
  require('./router/chat')(app);
  require('./router/system')(app);
  require('./router/invite')(app);
  require('./router/report')(app);
};
