module.exports = app => {
    const { router, io, controller } = app;
    const baseURL = app.config.baseURL;

    router.post(`${baseURL}/chat/sendVoiceMessage`,app.jwt, controller.chat.sendVoiceMessage);
    router.get(`${baseURL}/chat/getSentVoiceMessage`,app.jwt, controller.chat.getSentVoiceMessage);
    router.get(`${baseURL}/chat/getReceivedVoiceMessage`,app.jwt, controller.chat.getReceivedVoiceMessage);
  };
  