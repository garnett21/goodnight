module.exports = app => {
  const { router, controller } = app;
  const baseURL = app.config.baseURL;

  router.get(`${baseURL}/invite/getInvitation`, app.jwt, controller.invite.getInvitation);
  
};
