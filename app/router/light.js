module.exports = app => {
    const { router, controller } = app;
    const baseURL = app.config.baseURL;
    //灯模块..
    router.get(`${baseURL}/light/showCurrentLight`, app.jwt, controller.light.showCurrentLight);
    router.get(`${baseURL}/light/showAvailableLight`, app.jwt, controller.light.showAvailableLight);
    router.put(`${baseURL}/light/changeLight`, app.jwt, controller.light.changeLight);
    router.put(`${baseURL}/light/turnOffLight`, app.jwt, controller.light.turnOffLight);
    router.get(`${baseURL}/light/lightOffTime`, app.jwt, controller.light.lightOffTime);
    router.get(`${baseURL}/light/myLightOffTime`, app.jwt, controller.light.myLightOffTime);
    router.get(`${baseURL}/light/lightStatus`, app.jwt, controller.light.lightStatus);
    router.get(`${baseURL}/light/roomieLightOffTime`, app.jwt, controller.light.roomieLightOffTime);
};