module.exports = app => {
  const { router, io, controller } = app;
  const baseURL = app.config.baseURL;

  //匹配模块
  io.route('match', app.io.controller.match.processMatch);
  router.put(`${baseURL}/match/acquireMatchCode`, app.jwt, controller.match.acquireMatchCode);
  router.get(`${baseURL}/match/getMatchWindow`, app.jwt, controller.match.getMatchWindow);
  router.get(`${baseURL}/match/getCurrentMatch`, app.jwt, controller.match.getCurrentMatch);
  router.post(`${baseURL}/match/sendMatchRequest`, app.jwt, controller.match.sendMatchRequest);
  router.get(`${baseURL}/match/getReceivedMatchRequest`, app.jwt, controller.match.getReceivedMatchRequest);
  router.put(`${baseURL}/match/replyMatchRequest`, app.jwt, controller.match.replyMatchRequest);
  router.get(`${baseURL}/match/getSentMatchRequest`, app.jwt, controller.match.getSentMatchRequest);
  router.put(`${baseURL}/match/renewCurrentMatch`,app.jwt,controller.match.renewCurrentMatch);
};
