module.exports = app => {
    const { router, io, controller } = app;
    const baseURL = app.config.baseURL;

    //举报模块
    router.post(`${baseURL}/report/reportCurrentMatch`, app.jwt, controller.report.reportCurrentMatch);
};
