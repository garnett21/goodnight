module.exports = app => {
    const { router, controller } = app;
    const baseURL = app.config.baseURL;

    router.get(`${baseURL}/system/getSystemTime`,app.jwt, controller.system.getSystemTime);
    
  };
  