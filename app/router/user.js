module.exports = app => {
  const { router, controller } = app;
  const baseURL = app.config.baseURL;
  
  //用户模块
  router.post(`${baseURL}/user/miniapp/login`, controller.user.miniappLogin);
  router.post(`${baseURL}/user/mobile/login`, controller.user.mobileLogin);
  router.get(`${baseURL}/user/getUserInfo`, app.jwt, controller.user.getUserInfo);
  router.get(`${baseURL}/user/getUserInfoByMatchCode`, app.jwt, controller.user.getUserInfoByMatchCode);
  router.put(`${baseURL}/user/activateVIP`, app.jwt, controller.user.activateVIP);
  router.put(`${baseURL}/user/updateNickname`, app.jwt, controller.user.updateNickname);
  router.put(`${baseURL}/user/updateArea`, app.jwt, controller.user.updateArea);
};
