module.exports = app => {
    const { router, controller,middleware } = app;
    const baseURL = app.config.baseURL;
    const vipJudgement = middleware.vipJudgement();
    //const VIPJudgement = app.middleware.VIPJudgement();
    //语音模块
    router.get(`${baseURL}/voice/officialVoicePush`, app.jwt, controller.voice.officialVoicePush);
    router.get(`${baseURL}/voice/reviewOldVoices`, app.jwt, controller.voice.reviewOldVoices);
    router.get(`${baseURL}/voice/vipVoicePush`, app.jwt,vipJudgement, controller.voice.vipVoicePush);
    router.post(`${baseURL}/voice/uploadAudio`, app.jwt, controller.voice.uploadAudio);
};