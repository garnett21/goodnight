module.exports = app => {
  return {
    schedule: {
      cron: app.config.schedule.deactivatePendingRequest.cron,
      type: app.config.schedule.deactivatePendingRequest.type,
      disable: app.config.schedule.deactivatePendingRequest.disable,
    },
    
    async task(ctx) {
      await ctx.model.MatchRequest.deactivateAllPendingMatchRequest();
    },
  };
};