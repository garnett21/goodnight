module.exports = app => {
  return {
    schedule: {
      cron: app.config.schedule.renewCurrentMatchRequest.cron,
      type: app.config.schedule.renewCurrentMatchRequest.type,
      disable: app.config.schedule.renewCurrentMatchRequest.disable,
    },

    async task(ctx) {
      let renewMap = new Map();
      let willingToRenew = await ctx.model.MatchCurrent.getAllRecordWillingToRenew();
      let beginTime = app.config.matchWindow.beginTime;
      let renewTime = `${ctx.helper.dateFormater(new Date())} ${beginTime}`;

      for (const renewRecord of willingToRenew) {
        renewMap.set(renewRecord.unionId, renewRecord.matchId);
      }

      let renewList = [];
      let matchHistory = [];

      for (let [unionId, matchId] of renewMap) {
        if (renewMap.get(matchId) === unionId) {

          //更改renew状态
          renewList.push(unionId);
          renewList.push(matchId);

          //将一对匹配加入记录历史中
          matchHistory.push({
            unionId: unionId,
            matchId: matchId,
            createdAt: renewTime,
          });

          matchHistory.push({
            unionId: matchId,
            matchId: unionId,
            createdAt: renewTime,
          });

          //防止重复记录
          renewMap.delete(unionId);
          renewMap.delete(matchId);
        }
      }

      if (renewList.length > 0) {
        await ctx.model.MatchCurrent.renewCurrentMatch(renewList, renewTime);
        await ctx.model.MatchHistory.createMatchHistoryRecordViaRenew(matchHistory);
      }
    },
  };
};