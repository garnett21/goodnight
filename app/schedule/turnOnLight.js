module.exports = app => {
    return {
      schedule: {
        cron: app.config.schedule.autoTurnOnLight.cron,
        type: app.config.schedule.autoTurnOnLight.type,
        disable: app.config.schedule.autoTurnOnLight.disable,
        // cron:'0 30 5 * * *',
        // type:'worker',
        // disable:false
      },
      
      async task(ctx) {
        await ctx.model.Userlight.turnOnLight();
      },
    };
  };