const Service = require('egg').Service;
const OSS = require('ali-oss');
const uuidv4 = require('uuid/v4');
const ChatErrorInfo = require('../error/errorInfo/chat');
const HttpCustomError = require('../error/HttpCustomError');
const VoiceMessageWrapper = require('../wrapper/voiceMessage');

class ChatService extends Service {
    async getReceivedVoiceMessage(receiverId) {
        const { ctx, service, app } = this;
        let receivedVoiceMessages = [];
        let searchWindow = await app.redis.get(`voiceMessageWindow@${receiverId}`);
        if (!searchWindow) {
            let refreshWindow = await service.match.getRefreshWindow();
            searchWindow = await this.computeVoiceMessageSearchWindow(receiverId, refreshWindow);

            //refreshWindow结束时间距离夜晚22点之间的时间补偿,30秒
            let compensationSeconds = 30000;
            let TTLseconds = Math.ceil((new Date(refreshWindow.endTime).getTime() + compensationSeconds - Date.now()) / 1000);
            await app.redis.set(`voiceMessageWindow@${receiverId}`, JSON.stringify(searchWindow), 'ex', TTLseconds);

            receivedVoiceMessages = await ctx.model.VoiceMessage.getReceivedVoiceMessageWithinWindow(receiverId, searchWindow);
            for (let i = 0; i < receivedVoiceMessages.length; i++) {
                receivedVoiceMessages[i] = VoiceMessageWrapper.voiceMessageWrapper(receivedVoiceMessages[i]);
            }
        }
        else {
            searchWindow = JSON.parse(searchWindow);
            receivedVoiceMessages = await ctx.model.VoiceMessage.getReceivedVoiceMessageWithinWindow(receiverId, searchWindow);
            for (let i = 0; i < receivedVoiceMessages.length; i++) {
                receivedVoiceMessages[i] = VoiceMessageWrapper.voiceMessageWrapper(receivedVoiceMessages[i]);
            }
        }

        return receivedVoiceMessages;
    }

    async getSentVoiceMessage(senderId) {
        const { ctx, service, app } = this;
        let sentVoiceMessages = [];
        let searchWindow = await app.redis.get(`voiceMessageWindow@${senderId}`);

        if (!searchWindow) {
            let refreshWindow = await service.match.getRefreshWindow();
            searchWindow = await this.computeVoiceMessageSearchWindow(senderId, refreshWindow);

            //refreshWindow结束时间距离夜晚22点之间的时间补偿,30秒
            let compensationSeconds = 30000;
            let TTLseconds = Math.ceil((new Date(refreshWindow.endTime).getTime() + compensationSeconds - Date.now()) / 1000);
            await app.redis.set(`voiceMessageWindow@${senderId}`, JSON.stringify(searchWindow), 'ex', TTLseconds);

            sentVoiceMessages = await ctx.model.VoiceMessage.getSentVoiceMessageWithinWindow(senderId, searchWindow);
            for (let i = 0; i < sentVoiceMessages.length; i++) {
                sentVoiceMessages[i] = VoiceMessageWrapper.voiceMessageWrapper(sentVoiceMessages[i]);
            }
        }
        else {
            searchWindow = JSON.parse(searchWindow);
            sentVoiceMessages = await ctx.model.VoiceMessage.getSentVoiceMessageWithinWindow(senderId, searchWindow);
            for (let i = 0; i < sentVoiceMessages.length; i++) {
                sentVoiceMessages[i] = VoiceMessageWrapper.voiceMessageWrapper(sentVoiceMessages[i]);
            }

        }

        return sentVoiceMessages;
    }

    async computeVoiceMessageSearchWindow(unionId, refreshWindow) {
        const { ctx } = this;
        let matchHisotry = await ctx.model.MatchHistory.getMatchHistoryForVoiceMessage(unionId);
        let latestMatch = matchHisotry[0];
        if (ctx.helper.isTimestampWithinMatchWindow(latestMatch.createdAt, refreshWindow)) {
            //第零天22点到第二天10点,36小时
            const timeDiff = 129600 * 1000;
            let consecutiveMatch = [];
            consecutiveMatch.push(latestMatch);

            for (let i = 1; i < matchHisotry.length; i++) {
                if (matchHisotry[i].matchId === matchHisotry[i - 1].matchId) {

                    let latterMatchTime = new Date(matchHisotry[i - 1].createdAt).getTime();
                    let previousMatchTime = new Date(matchHisotry[i].createdAt).getTime();
                    let matchTimeDiff = latterMatchTime - previousMatchTime;
                    if (matchTimeDiff < timeDiff) {
                        consecutiveMatch.push(matchHisotry[i]);
                    }
                    else {
                        break;
                    }
                }
                else {
                    break;
                }
            }

            let searchWindow = {
                beginTime: consecutiveMatch[consecutiveMatch.length - 1].createdAt,
                endTime: refreshWindow.endTime,
            }

            return searchWindow;
        }

        return refreshWindow;
    }

    async sendVoiceMessage(senderId, platform) {
        const { ctx, app } = this;

        let receiverId = await this.isAllowToSendVoiceMessage(senderId, platform);

        let client = new OSS({
            region: app.config.alioss.region,
            accessKeyId: app.config.alioss.accessKeyId,
            accessKeySecret: app.config.alioss.accessKeySecret,
            bucket: app.config.alioss.bucket
        });
        let stream = await ctx.getFileStream();
        let filename = stream.filename;
        let extension = filename.substring(filename.length - 3, filename.length);
        let duration = ctx.request.query.duration;
        let messageId = uuidv4();
        let uploadResult = await client.putStream(`voice_message/${messageId}.${extension}`, stream);
        if (uploadResult.res.status === 200) {
            let message = {
                messageId: messageId,
                senderId: senderId,
                receiverId: receiverId,
                duration: duration,
            }
            await ctx.model.VoiceMessage.createVoiceMessage(message);
            return { receiverId, messageId };
        }
        else {
            throw new HttpCustomError(ChatErrorInfo.OSS_UPLOAD_ERROR);
        }
    }

    async isAllowToSendVoiceMessage(senderId, platform) {
        const { ctx, service } = this;
        const MINIAPP_DAILY_MSG_LIMIT = 1;
        if (platform === 'miniapp') {
            let refreshWindow = await service.match.getRefreshWindow();
            if (!ctx.helper.isNowWithinMatchWindow(refreshWindow)) {
                throw new HttpCustomError(ChatErrorInfo.MINIAPP_MSG_COUNT_EXCEED_DAILY_LIMIT);
            }
            let count = (await ctx.model.VoiceMessage.getSentVoiceMessageWithinWindow(senderId, refreshWindow)).length;
            if (count >= MINIAPP_DAILY_MSG_LIMIT) {
                throw new HttpCustomError(ChatErrorInfo.MINIAPP_MSG_COUNT_EXCEED_DAILY_LIMIT);
            }
        }

        let receiverId = await service.match.getCurrentMatchUnionId(senderId);
        if (!receiverId) {
            throw new HttpCustomError(ChatErrorInfo.CURRENT_MATCH_NOT_EXIST);
        }

        return receiverId;
    }
}

module.exports = ChatService;