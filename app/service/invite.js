const Service = require('egg').Service;
const uuidv4 = require('uuid/v4');
const InviteWrapper = require('../wrapper/invite');

class InviteService extends Service {

    async getInvitation(unionId) {
        const { ctx } = this;

        let now = new Date();
        let invitation = await ctx.model.UserInvitation.getLatestInvitationByUnionId(unionId);
        let invitee = [];

        if (!invitation || new Date(invitation.expireAt) < now) {
            //29日有效期
            const validTime = 86400000 * 29;
            let invitationId = uuidv4();
            let expireAt = new Date(now.getTime() + validTime);
            await ctx.model.UserInvitation.createInvitation(unionId, invitationId, expireAt);
            invitation = await ctx.model.UserInvitation.getLatestInvitationByUnionId(unionId);
        }
        else {
            let inviteeRecord = await ctx.model.UserInviteHistory.getOldestThreeInvitationRecordByInvitationId(invitation.invitationId);
            if (inviteeRecord) {
                for (let i = 0; i < inviteeRecord.length; i++) {
                    invitee[i] = InviteWrapper.inviteeWrapper(await ctx.model.Userinfo.getUserByUnionId(inviteeRecord[i].inviteeId));
                }
            }
        }

        invitation = InviteWrapper.invitationWrapper(invitation, invitee);
        return invitation;
    }

    async completeInvitation(invitationId, inviteeId) {
        const { ctx } = this;
        const INVITATION_NOT_COMPLETED = 0;
        let invitation = await ctx.model.UserInvitation.getInvitationByInvitationId(invitationId);
        let now = new Date();
   
        if (invitation && invitation.completed === INVITATION_NOT_COMPLETED && now < new Date(invitation.expireAt)) {
            const REQUIRE_INVITEE_NUMBER = 3;
            await ctx.model.UserInviteHistory.createInvitationRecord(invitationId, inviteeId);
            let issuedInvitationCount = await ctx.model.UserInviteHistory.getIssuedInvitationRecordCount(invitationId);

            //满3人就升级时间
            if (issuedInvitationCount === REQUIRE_INVITEE_NUMBER) {
                //30日
                const validTime = 86400000 * 30;
                let inviterId = invitation.unionId;
                let accessRecord = await ctx.model.UserBetaAccess.getAccessRecordByUnionId(inviterId);
                let accessExpireAt = new Date(accessRecord.expireAt);
                
                if (accessExpireAt > now) {
                    let expireAt = new Date(accessExpireAt.getTime() + validTime);
                    await ctx.model.UserBetaAccess.updateAccessRecord(inviterId, expireAt);

                }
                else if (now >= accessExpireAt) {
                    let expireAt = new Date(now.getTime() + validTime);
                    await ctx.model.UserBetaAccess.updateAccessRecord(inviterId, expireAt);
                }

                await ctx.model.UserInvitation.updateInvitationCompleteStatus(invitationId);
            }

        }
    }
}

module.exports = InviteService;