const Service = require('egg').Service;
const lightErrorInfo = require('../error/errorInfo/light');
const HttpCustomError = require('../error/HttpCustomError');

class LightService extends Service {
    //展示目前可用的灯
    async showAvailableLight() {
        const { ctx } = this;
        let showAvailableLight = await ctx.model.Light.showAvailableLight();
        if (showAvailableLight.length != 0) {
            return showAvailableLight;
        } else {
            throw new HttpCustomError(lightErrorInfo.NO_AVAILABLE_LIGHTS);
        }
    };


    //查看目前使用的灯的皮肤url
    async picurlOfCurrentLight(lightId) {
        const { ctx } = this;
        return await ctx.model.Light.picurlOfCurrentLight(lightId);
    };


    //展示现在在使用的灯
    async showCurrentLight(unionId) {
        const { ctx } = this;
        return await ctx.model.Userlight.showCurrentLight(unionId);
    };

    //当前用户的灯的状态
    async lightStatus(unionId) {
        const { ctx } = this;
        return await ctx.model.Userlight.lightStatus(unionId);
    };

    //查看室友的关灯时间
    async roomieLightOffTime(roomieUnionId) {
        const { ctx } = this;
        if (roomieUnionId == null) {
            throw new HttpCustomError(lightErrorInfo.NO_ROOMIE);
        } else {
            let unionId = roomieUnionId;
            let roomieLightOffTime = await ctx.model.Turn.myLightOffTime(unionId);
            if (roomieLightOffTime.length == 0) {
                throw new HttpCustomError(lightErrorInfo.NO_EOOMIE_RECORDS);
            } else {
                return roomieLightOffTime;
            };
        };
    };


    //改变灯皮肤
    async changeLight(unionId, lightId) {
        const { ctx } = this;
        let isVIP = await this.vipJudgement(unionId);
        if (isVIP == 1) {//是VIP
            let availableLight = await ctx.model.Light.showAvailableLightId();
            let mySet = new Set();
            for (let i = 0; i < availableLight.length; i++) {
                let record = JSON.stringify(availableLight[i]);
                let Record = record.match(/{"lightId":(\S*)}/)[1];
                mySet.add(Record);
            };
            let isLightIdCorrect = mySet.has(lightId);
            let isRecordExist = await ctx.model.Userlight.isRecordExist(unionId);
            if (isRecordExist == undefined) {//无记录
                let status = 1;
                return await ctx.model.Userlight.newLight(unionId, lightId, status);
            } else {//有记录
                if (isLightIdCorrect == true) {
                    return await ctx.model.Userlight.changeLight(unionId, lightId);
                } else {
                    throw new HttpCustomError(lightErrorInfo.CHANGE_FAIL);
                };
            }
        };
        if (isVIP == 0) {//不是VIP
            let availableLight = await ctx.model.Light.showAvailableNotVipLightId();
            let mySet = new Set();
            for (let i = 0; i < availableLight.length; i++) {
                let record = JSON.stringify(availableLight[i]);
                let Record = record.match(/{"lightId":(\S*)}/)[1];
                mySet.add(Record);
            };
            let isLightIdCorrect = mySet.has(lightId);
            let isRecordExist = await ctx.model.Userlight.isRecordExist(unionId);
            if (isRecordExist == undefined) {//无记录
                let status = 1;
                return await ctx.model.Userlight.newLight(unionId, lightId, status);
            } else {//有记录
                if (isLightIdCorrect == true) {
                    return await ctx.model.Userlight.changeLight(unionId, lightId);
                } else {
                    throw new HttpCustomError(lightErrorInfo.CHANGE_FAIL);
                };
            }
        };

    };



    //把用户关灯的状态写入数据库
    async turnOffLightStatus(unionId) {
        const { ctx } = this;
        let isRecordExist = await ctx.model.Userlight.isRecordExist(unionId);
        if (isRecordExist == undefined) {
            let currentLightMessage = await ctx.model.Userlight.showCurrentLight(unionId);
            let lightId = currentLightMessage.lightId;
            let status = 0;
            await ctx.model.Userlight.turnOffNewLight(unionId, lightId, status);
        } else {
            return await ctx.model.Userlight.turnOffLight(unionId);
        };

    };


    //把用户关灯的时间写入数据库
    async  turnOffLightTime(unionId) {
        let findRecords;
        const { ctx, service } = this;
        let lightWindow = await this.getLightWindow();
        let beginTime = lightWindow.beginTime;
        let endTime = lightWindow.endTime;
        let isOrNotTheLightOffTime = await this.isOrNotTheLightOffTime(beginTime, endTime);
        if (isOrNotTheLightOffTime == true) {//在关灯时间段内
            findRecords = await ctx.model.Turn.findRecords(unionId, beginTime, endTime);
        } else {//不在关灯时间段内
            throw new HttpCustomError(lightErrorInfo.NOT_CORRECT_TIME_FOR_TURNOFFLIGHT);
        };
        if (findRecords == 0) {//无记录
            let currentMatchId = await service.match.getCurrentMatchUnionId(unionId);
            await ctx.model.Turn.turnOffLight(unionId, new Date());
            return currentMatchId;
        } else {//有记录
            throw new HttpCustomError(lightErrorInfo.CAN_NOT_TURN_OFF_AGAIN);
        };
    };

    //查看我的睡眠记录
    async myLightOffTime(unionId) {
        const { ctx } = this;
        let myLightOffTime = await ctx.model.Turn.myLightOffTime(unionId);
        if (myLightOffTime.length == 0) {
            throw new HttpCustomError(lightErrorInfo.CAN_NOT_FIND_MY_RECORDS);
        } else {
            return myLightOffTime;
        };

    };

    //从数据库读取昨晚关灯的用户数...
    async lightOffTime() {
        const { ctx } = this;
        let lightWindow = await this.getLightWindow();
        let beginTime = lightWindow.beginTime;
        let endTime = lightWindow.endTime;
        let isOrNotTheLightOffTime = await this.isOrNotTheLightOffTime(beginTime, endTime);
        console.log(isOrNotTheLightOffTime);
        if (isOrNotTheLightOffTime == true) {
            return await ctx.model.Turn.countRecords(beginTime, endTime);
        } else {
            throw new HttpCustomError(lightErrorInfo.NOT_CORRECT_TIME_FOR_QUERYING);
        };

    };


    //适配开始和结束时间
    async getLightWindow() {
        const { ctx, app } = this;
        let now = new Date();

        let lightWindow = {
            beginTime: undefined,
            endTime: undefined,
        }

        let beginHour = parseInt(app.config.lightWindow.beginTime.substring(0, 2));
        if (now.getHours() >= beginHour && now.getHours() < 24) {
            let tomorrow = ctx.helper.getTomorrow();
            lightWindow.beginTime = `${ctx.helper.dateFormater(now)} ${app.config.lightWindow.beginTime}`;
            lightWindow.endTime = `${ctx.helper.dateFormater(tomorrow)} ${app.config.lightWindow.endTime}`;

        }
        else {
            let yesterday = ctx.helper.getYesterday();
            lightWindow.beginTime = `${ctx.helper.dateFormater(yesterday)} ${app.config.lightWindow.beginTime}`;
            lightWindow.endTime = `${ctx.helper.dateFormater(now)} ${app.config.lightWindow.endTime}`;

        }

        return lightWindow;
    };


    //判断是否在关灯时间段内..
    async isOrNotTheLightOffTime(beginTime, endTime) {
        const { ctx, app } = this;
        let date = new Date();
        let beginTimeOfDate = new Date(beginTime);
        let endTimeOfDate = new Date(endTime);
        let currentHour = date.getHours();
        let currentMin = date.getMinutes();
        let beginHour = beginTimeOfDate.getHours();
        let endHour = endTimeOfDate.getHours();
        let endMin = endTimeOfDate.getMinutes();
        if ((currentHour > endHour && currentHour < beginHour) || (currentHour == endHour && currentMin >= endMin && currentHour < beginHour)) {
            return false;
        } else {
            return true;
        };

    };


    //判断是不是VIP
    async vipJudgement(unionId) {
        const { ctx } = this;
        let userinfo = await ctx.model.Userinfo.getUserByUnionId(unionId);
        let vipExpire = userinfo.vip_expire;
        let now = new Date();
        let expireDay = new Date(vipExpire);
        let currentTime = now.getTime();
        let expireTime = expireDay.getTime();
        if (vipExpire == null) {
            return 0
        } else {
            if (currentTime <= expireTime) {
                return 1;
            } else {
                return 0;
            };
        }
    };


};
module.exports = LightService;