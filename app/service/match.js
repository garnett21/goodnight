const Service = require('egg').Service;
const UserInfoWrapper = require('../wrapper/userInfo');
const LightWrapper = require('../wrapper/light');
const MatchErrorInfo = require('../error/errorInfo/match');
const HttpCustomError = require('../error/HttpCustomError');
const SocketCustomError = require('../error/SocketCustomError');

class MatchService extends Service {

    async enqueue(unionId, socketId) {
        const { app } = this;
        const TTLmillisecond = app.config.matchTTL;
        const TTLsecond = TTLmillisecond / 1000;
        const sortedSet = 'matchSortedSet';
        let matchData = {
            unionId: unionId,
            socketId: socketId
        }
        if (await app.redis.get(`matchQueue@${unionId}`) === null) {
            let current = new Date();
            let expireTimestamp = new Date(current.getTime() + TTLmillisecond).getTime();
            await app.redis.set(`matchQueue@${unionId}`, unionId, 'ex', TTLsecond);
            await app.redis.zadd(sortedSet, [expireTimestamp, JSON.stringify(matchData)]);
            return true;
        }

        throw new SocketCustomError(socketId, app.config.socketChannel.randomMatch, MatchErrorInfo.RANDOM_EXIST_IN_MATCH_QUEUE);
    }

    async dequeueFront() {
        const sortedSet = 'matchSortedSet';
        const { app } = this;
        const currentTimestamp = new Date().getTime();
        let matches = [];
        let remove = [];
        while (await app.redis.zcard(sortedSet) >= 2 && matches.length < 2) {
            let preMatch = await app.redis.zrange(sortedSet, 0, 1, 'withscores');
            for (let i = 1; i < preMatch.length; i = i + 2) {
                //凑成一对后停止匹配
                if (matches.length === 2) {
                    break;
                }
                let expireTimestamp = preMatch[i];
                if (expireTimestamp >= currentTimestamp) {
                    matches.push(preMatch[i - 1]);
                }
                else {
                    remove.push(preMatch[i - 1]);
                }
            }

            if (remove.length > 0) {
                //移除已过时元素
                await app.redis.zrem(sortedSet, remove);
            }
        }
        if (matches.length === 2) {
            //移除已匹配元素
            await app.redis.zrem(sortedSet, matches);
            return await this.matchComplete(matches);
        }
        return null;
    }

    async matchComplete(matches) {
        const { ctx } = this;
        for (let i = 0; i < matches.length; i++) {
            let match = {
                unionId: JSON.parse(matches[i]).unionId,
                socketId: JSON.parse(matches[i]).socketId,
            }
            matches[i] = match;
        }
        let matchResult = [];
        for (let i = 0; i < matches.length; i++) {
            let self = matches[i];
            let currentMatchExist = await ctx.model.MatchCurrent.isCurrentMatchExist(self.unionId);
            let match = i === 0 ? matches[i + 1] : matches[i - 1];
            if (currentMatchExist) {
                let renew = 0;
                await ctx.model.MatchCurrent.updateCurrentMatch(self.unionId, match.unionId, renew);
            }
            else {
                await ctx.model.MatchCurrent.createCurrentMatch(self.unionId, match.unionId);
            }

            await ctx.model.MatchHistory.createMatchHistoryRecord(self.unionId, match.unionId);
            await ctx.model.MatchRequest.deactivateAllPendingMatchRequestByUnionId(self.unionId);
            let selfInfo = await ctx.model.Userinfo.getUserByUnionId(self.unionId);
            let selfInfoWrapper = UserInfoWrapper.matchUserInfoWrapper(selfInfo);
            selfInfoWrapper['socketId'] = self.socketId;
            matchResult.push(selfInfoWrapper);
        }
        return matchResult;
    }

    async acquireMatchCode(unionId) {
        const { ctx, service } = this;

        if (!await ctx.model.Userinfo.isMatchCodeExist(unionId)) {
            let assignedCode = await ctx.model.MatchCode.assignMatchCode();
            if (assignedCode !== null) {
                let updateResult = await ctx.model.Userinfo.updateMatchCode(unionId, assignedCode);
                if (updateResult[0] === 1) {
                    //公测准入资格
                    await service.user.publicBetaAccess(unionId);

                    return assignedCode;
                }
            }
        }
        throw new HttpCustomError(MatchErrorInfo.ASSIGN_MATCH_CODE_ERROR);
    }

    async isAllowToMatch(unionId, socketId) {
        const { ctx, app } = this;
        let matchWindow = await this.getMatchWindow();
        let refreshWindow = await this.getRefreshWindow();
        //不在有效匹配时间窗口之中
        if (!ctx.helper.isNowWithinMatchWindow(matchWindow)) {
            throw new SocketCustomError(socketId, app.config.socketChannel.randomMatch, MatchErrorInfo.NOW_IS_NOT_IN_MATCH_WINDOW);
        }

        //用户存在于封停名单之中
        let isUserExistInBannedList = await ctx.model.MatchBannedList.isUserExistInBannedList(unionId);
        if (isUserExistInBannedList) {
            throw new SocketCustomError(socketId, app.config.socketChannel.randomMatch, MatchErrorInfo.USER_EXIST_IN_BANNED_LIST);
        }

        let currentMatchExist = await ctx.model.MatchCurrent.isCurrentMatchExist(unionId);
        //无当前匹配记录
        if (!currentMatchExist) {
            return true;
        }
        let latestCurrentMatch = await ctx.model.MatchCurrent.getCurrentMatchByUnionId(unionId);
        let latestMatchTime = new Date(latestCurrentMatch.updatedAt);

        //在当前刷新窗口找到匹配记录
        if (currentMatchExist && ctx.helper.isTimestampWithinMatchWindow(latestMatchTime, refreshWindow)) {
            throw new SocketCustomError(socketId, app.config.socketChannel.randomMatch, MatchErrorInfo.RANDOM_MATCH_ALREADY_EXIST);
        }

        return true;
    }

    async getMatchWindow() {
        const { ctx, app } = this;
        let now = new Date();

        let matchWindow = {
            beginTime: undefined,
            endTime: undefined,
        }

        let beginHour = parseInt(app.config.matchWindow.beginTime.substring(0, 2));
        if (now.getHours() >= beginHour && now.getHours() < 24) {
            let tomorrow = ctx.helper.getTomorrow();
            matchWindow.beginTime = `${ctx.helper.dateFormater(now)} ${app.config.matchWindow.beginTime}`;
            matchWindow.endTime = `${ctx.helper.dateFormater(tomorrow)} ${app.config.matchWindow.endTime}`;
        }
        else {
            let yesterday = ctx.helper.getYesterday();
            matchWindow.beginTime = `${ctx.helper.dateFormater(yesterday)} ${app.config.matchWindow.beginTime}`;
            matchWindow.endTime = `${ctx.helper.dateFormater(now)} ${app.config.matchWindow.endTime}`;
        }

        return matchWindow;
    }

    async getRefreshWindow() {
        const { ctx, app } = this;
        let now = new Date();
        let refreshWindow = {
            beginTime: undefined,
            endTime: undefined,
        }

        let beginHour = parseInt(app.config.refreshWindow.beginTime.substring(0, 2));
        if (now.getHours() >= beginHour && now.getHours() < 24) {
            let tomorrow = ctx.helper.getTomorrow();
            refreshWindow.beginTime = `${ctx.helper.dateFormater(now)} ${app.config.refreshWindow.beginTime}`;
            refreshWindow.endTime = `${ctx.helper.dateFormater(tomorrow)} ${app.config.refreshWindow.endTime}`;
        }
        else {
            let yesterday = ctx.helper.getYesterday();
            refreshWindow.beginTime = `${ctx.helper.dateFormater(yesterday)} ${app.config.refreshWindow.beginTime}`;
            refreshWindow.endTime = `${ctx.helper.dateFormater(now)} ${app.config.refreshWindow.endTime}`;
        }

        return refreshWindow;
    }

    async getCurrentMatch(unionId) {
        const { ctx, service } = this;
        let latestMatch = await ctx.model.MatchCurrent.getCurrentMatchByUnionId(unionId);
        if (latestMatch === null) {
            return null;
        }
        else {
            let refreshWindow = await this.getRefreshWindow();
            if (ctx.helper.isTimestampWithinMatchWindow(latestMatch.updatedAt, refreshWindow)) {
                let currentMatch = await ctx.model.Userinfo.getUserByUnionId(latestMatch.matchId);
                let matchCurrentLight = await service.light.showCurrentLight(latestMatch.matchId);
                let matchCurrentLightInfo = await service.light.picurlOfCurrentLight(matchCurrentLight.lightId);
                let lightWrapper = LightWrapper.lightWrapper(matchCurrentLight, matchCurrentLightInfo);
                let currentMatchWrapper = UserInfoWrapper.matchUserInfoWrapper(currentMatch, lightWrapper);
                return currentMatchWrapper;
            }
        }
        return null;
    }

    async getCurrentMatchUnionId(unionId) {
        const { ctx } = this;
        let latestMatch = await ctx.model.MatchCurrent.getCurrentMatchByUnionId(unionId);
        if (latestMatch === null) {
            return null;
        }
        else {
            let refreshWindow = await this.getRefreshWindow();
            if (ctx.helper.isTimestampWithinMatchWindow(latestMatch.updatedAt, refreshWindow)) {
                let currentMatch = await ctx.model.Userinfo.getUserByUnionId(latestMatch.matchId);
                return currentMatch.unionId;
            }
        }
        return null;
    }

    async renewCurrentMatch(unionId) {
        const { ctx } = this;
        let updateResult = await ctx.model.MatchCurrent.setRenewStatus(unionId);
        if (updateResult[0] === 0) {
            throw new HttpCustomError(MatchErrorInfo.CURRENT_MATCH_ALREADY_RENEWED);
        }
    }
}

module.exports = MatchService;
