const Service = require('egg').Service;
const MatchRequestWrapper = require('../wrapper/matchRequest');
const MatchErrorInfo = require('../error/errorInfo/match');
const HttpCustomError = require('../error/HttpCustomError');

class MatchRequestService extends Service {

    async isAllowToReplyMatchRequest(receiverId, requestId, replyStatus) {
        const { ctx, app, service } = this;
        let refreshWindow = await service.match.getRefreshWindow();
        let currentMatchExist = await ctx.model.MatchCurrent.isCurrentMatchExist(receiverId);
        const ACCEPT_STATUS = 1;
        const REJECT_STATUS = -1;

        //接收者位于封停名单中
        let isReceiverExistInBannedList = await ctx.model.MatchBannedList.isUserExistInBannedList(receiverId);
        if (isReceiverExistInBannedList) {
            throw new HttpCustomError(MatchErrorInfo.REQUEST_RECEIVER_EXIST_IN_BANNED_LIST);
        }

        //回复状态码错误
        if (replyStatus !== ACCEPT_STATUS && replyStatus !== REJECT_STATUS) {
            throw new HttpCustomError(MatchErrorInfo.REPLY_STATUS_CODE_ERROR);
        }

        //回复者在当前刷新窗口找到匹配记录
        if (currentMatchExist) {
            let latestCurrentMatch = await ctx.model.MatchCurrent.getCurrentMatchByUnionId(receiverId);
            let latestMatchTime = new Date(latestCurrentMatch.updatedAt);
            if (ctx.helper.isTimestampWithinMatchWindow(latestMatchTime, refreshWindow)) {
                throw new HttpCustomError(MatchErrorInfo.RECEIVER_MATCH_ALREADY_EXIST);
            }
        }

        //在匹配队列中找到回复者
        if (await app.redis.get(`matchQueue@${receiverId}`)) {
            throw new HttpCustomError(MatchErrorInfo.RECEIVER_EXIST_IN_MATCH_QUEUE);
        }

        //找不到匹配请求
        let matchRequest = await ctx.model.MatchRequest.getMatchRequestByRequestId(requestId);
        if (!matchRequest) {
            throw new HttpCustomError(MatchErrorInfo.MATCH_REQUEST_NOT_FOUND);
        }


        let senderId = matchRequest.senderId;

        //发送者位于封停名单中
        let isSenderExistInBannedList = await ctx.model.MatchBannedList.isUserExistInBannedList(senderId);
        if (isSenderExistInBannedList) {
            throw new HttpCustomError(MatchErrorInfo.REQUEST_SENDER_EXIST_IN_BANNED_LIST);
        }

        //在匹配队列中找到发送者
        if (await app.redis.get(`matchQueue@${senderId}`)) {
            throw new HttpCustomError(MatchErrorInfo.SENDER_EXIST_IN_MATCH_QUEUE);
        }

        //发送者现在刷新窗口内存在匹配
        if (await service.match.getCurrentMatch(senderId)) {
            throw new HttpCustomError(MatchErrorInfo.SENDER_MATCH_ALREADY_EXIST);
        }
        return senderId;
    }

    async isAllowToSendMatchRequest(senderId, matchWindow, refreshWindow) {
        const { ctx, app } = this;
        const requestLimit = 3;
        //当前时间不在匹配窗口
        if (!ctx.helper.isNowWithinMatchWindow(matchWindow)) {
            throw new HttpCustomError(MatchErrorInfo.NOW_IS_NOT_IN_MATCH_WINDOW);
        }

        //发送者存在于封停名单
        let isSenderExistInBannedList = await ctx.model.MatchBannedList.isUserExistInBannedList(senderId);
        if (isSenderExistInBannedList) {
            throw new HttpCustomError(MatchErrorInfo.REQUEST_SENDER_EXIST_IN_BANNED_LIST);
        }

        //每日请求数量超出限制
        let sentRequestCount = await ctx.model.MatchRequest.getSentRequestCountWithinCurrentMatchWindow(senderId, matchWindow);
        if (sentRequestCount >= requestLimit) {
            throw new HttpCustomError(MatchErrorInfo.REQUEST_COUNT_EXCEED_DAILY_LIMIT);
        }

        //发送者在随机匹配队列中
        if (await app.redis.get(`matchQueue@${senderId}`)) {
            throw new HttpCustomError(MatchErrorInfo.SENDER_EXIST_IN_MATCH_QUEUE);
        }

        //match code找不到人
        let code = ctx.request.body.code;
        let receiver = await ctx.model.Userinfo.getUserByMatchCode(code);
        if (receiver === null) {
            throw new HttpCustomError(MatchErrorInfo.MATCH_CODE_NOT_FOUND);
        }
        else if (receiver.unionId === senderId) {
            throw new HttpCustomError(MatchErrorInfo.MATCH_CODE_BELONGS_TO_SENDER);
        }
        let receiverId = receiver.unionId;

        //接收者存在于封停名单
        let isReceiverExistInBannedList = await ctx.model.MatchBannedList.isUserExistInBannedList(receiverId);
        if (isReceiverExistInBannedList) {
            throw new HttpCustomError(MatchErrorInfo.REQUEST_RECEIVER_EXIST_IN_BANNED_LIST);
        }

        //接收者在随机匹配队列中
        if (await app.redis.get(`matchQueue@${receiverId}`)) {
            throw new HttpCustomError(MatchErrorInfo.RECEIVER_EXIST_IN_MATCH_QUEUE);
        }

        //接收者在当前刷新窗口中已经有匹配
        let receiverCurrentMatch = await ctx.model.MatchCurrent.getCurrentMatchByUnionId(receiver.unionId);
        if (receiverCurrentMatch !== null && ctx.helper.isTimestampWithinMatchWindow(receiverCurrentMatch.updatedAt, refreshWindow)) {
            throw new HttpCustomError(MatchErrorInfo.RECEIVER_MATCH_ALREADY_EXIST);
        }

        //最新一条匹配请求是在当前刷新窗口中
        let latestRequest = await ctx.model.MatchRequest.getLatestRequestBySenderIdAndReceiverId(senderId, receiverId);
        if (latestRequest !== null && ctx.helper.isTimestampWithinMatchWindow(latestRequest.createdAt, matchWindow)) {
            throw new HttpCustomError(MatchErrorInfo.LATEST_MATCH_REQUEST_WITHIN_MATCH_WINDOW);
        }

        return receiverId;
    }

    async sendMatchRequest(senderId) {
        const { ctx, app, service } = this;
        let matchWindow = await service.match.getMatchWindow();
        let refreshWindow = await service.match.getRefreshWindow();
        let receiverId = await this.isAllowToSendMatchRequest(senderId, matchWindow, refreshWindow);
        let now = new Date();
        let expireAt;
        let beginHour = parseInt(app.config.matchWindow.beginTime.substring(0, 2));
        if (now.getHours() >= beginHour) {
            let tomorrow = ctx.helper.getTomorrow();
            expireAt = `${ctx.helper.dateFormater(tomorrow)} ${app.config.matchWindow.beginTime}`;
        }
        else {
            expireAt = `${ctx.helper.dateFormater(now)} ${app.config.matchWindow.beginTime}`;
        }

        let requestId = await ctx.model.MatchRequest.createMatchRequest(senderId, receiverId, expireAt);
        let senderInfo = await ctx.model.Userinfo.getUserByUnionId(senderId);
        let sendRequestWrapper = MatchRequestWrapper.sendRequestWrapper(requestId, senderInfo);
        return { sendRequestWrapper, receiverId };

    }

    async getReceivedMatchRequest(receiverId) {
        const { ctx, service } = this;
        let matchWindow = await service.match.getMatchWindow();
        let receivedRequests = await ctx.model.MatchRequest.getReceivedReqeustBetweenMatchWindow(receiverId, matchWindow);
        for (let i = 0; i < receivedRequests.length; i++) {
            let senderInfo = await ctx.model.Userinfo.getUserByUnionId(receivedRequests[i].senderId);
            receivedRequests[i] = MatchRequestWrapper.receivedRequestWrapper(receivedRequests[i], senderInfo);
        }
        return receivedRequests;
    }

    async replyPendingMatchRequet(receiverId) {
        const { ctx } = this;
        const ACCEPT_STATUS = 1;
        let replyStatus = parseInt(ctx.request.body.replyStatus);
        let requestId = ctx.request.body.requestId;
        let senderId = await this.isAllowToReplyMatchRequest(receiverId, requestId, replyStatus);

        let updateRequestResult = await ctx.model.MatchRequest.updateReceivedPendingRequest(requestId, replyStatus, new Date());
        if (updateRequestResult[0] === 1) {
            if (replyStatus === ACCEPT_STATUS) {
                await this.completeReplyPendingMatchRequest(senderId, receiverId, requestId, false);
                return await this.completeReplyPendingMatchRequest(receiverId, senderId, requestId, true);
            }
        }
    }

    async completeReplyPendingMatchRequest(unionId, matchId, requestId, socketToSender) {
        const { ctx } = this;
        if (await ctx.model.MatchCurrent.isCurrentMatchExist(unionId)) {
            let renew = 0;
            await ctx.model.MatchCurrent.updateCurrentMatch(unionId, matchId, renew);
        }
        else {
            await ctx.model.MatchCurrent.createCurrentMatch(unionId, matchId);
        }
        await ctx.model.MatchHistory.createMatchHistoryRecord(unionId, matchId);
        await ctx.model.MatchRequest.deactivateAllPendingMatchRequestByUnionId(unionId);
        if (socketToSender) {
            let receiverInfo = await ctx.model.Userinfo.getUserByUnionId(unionId);
            let replyRequestWrapper = MatchRequestWrapper.replyRequestWrapper(requestId, receiverInfo);
            let senderId = matchId;
            return { replyRequestWrapper, senderId };
        }
    }

    async getSentMatchRequest(unionId) {
        const { ctx, service } = this;
        let pageNumber = parseInt(ctx.request.query.pageNumber);
        let pageSize = parseInt(ctx.request.query.pageSize);
        let matchWindow = await service.match.getMatchWindow();
        let sentRequests = await ctx.model.MatchRequest.getSentMatchRequestBetweenMatchWindow(unionId, pageNumber, pageSize, matchWindow);
        let sentRequestsWrapper = [];
        for (let i = 0; i < sentRequests.length; i++) {
            let receiverInfo = await ctx.model.Userinfo.getUserByUnionId(sentRequests[i].receiverId);
            sentRequestsWrapper.push(MatchRequestWrapper.sentRequestWrapper(sentRequests[i], receiverInfo));
        }
        return sentRequestsWrapper;
    }
}
module.exports = MatchRequestService;
