const Service = require('egg').Service;
const ReportErrorInfo = require('../error/errorInfo/report');
const HttpCustomError = require('../error/HttpCustomError');

class MatchService extends Service {

    async reportCurrentMatch(unionId) {
        const { ctx, service } = this;
        let suspectId = await service.match.getCurrentMatchUnionId(unionId);
        if (!suspectId) {
            throw new HttpCustomError(ReportErrorInfo.CURRENT_MATCH_BEING_REPORTED_NOT_EXIST);
        }

        let reason = ctx.request.body.reason;
        let category = parseInt(ctx.request.body.category);
        let refreshWindow = await service.match.getRefreshWindow();
        let isReportExist = await ctx.model.Report.isReportExistWithinWindow(unionId, refreshWindow);
        if (!isReportExist) {
            let receivedVoiceMessage = await service.chat.getReceivedVoiceMessage(unionId);
            if (receivedVoiceMessage.length === 0) {
                throw new HttpCustomError(ReportErrorInfo.NO_MESSAGE_RECEIVED_YET);
            }

            await ctx.model.Report.createReport(unionId, suspectId, reason, category);

            //标记被举报的语音
            let reportedMessages = [];
            for (let i = 0; i < receivedVoiceMessage.length; i++) {
                reportedMessages.push(receivedVoiceMessage[i].messageId);
            }
            await ctx.model.VoiceMessage.updateVoiceMessageAsBeingReported(reportedMessages);

        }
        else {
            throw new HttpCustomError(ReportErrorInfo.CURRENT_MATCH_ALREADY_BEING_REPORTED);
        }
    }
}

module.exports = MatchService;