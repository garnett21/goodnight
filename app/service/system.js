const Service = require('egg').Service;
class SystemService extends Service {
    async getSystemTime(){
        return new Date();
    }
}

module.exports = SystemService;