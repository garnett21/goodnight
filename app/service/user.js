const Service = require('egg').Service;
const axios = require('axios');
const WXBizDataCrypt = require('../extend/WXBizDataCrypt');
const UserInfoWrapper = require('../wrapper/userInfo');
const UserErrorInfo = require('../error/errorInfo/user');
const HttpCustomError = require('../error/HttpCustomError');

class UserService extends Service {

    async getMiniappLoginToken() {
        const { ctx, app, service } = this;
        let token;

        const platform = ctx.header.platform;
        let jsCode2SessionURL = app.config.jsCode2SessionURL;
        let code = ctx.request.body.code;
        let appId = app.config.miniapp.id;
        let appSecret = app.config.miniapp.secret;
        jsCode2SessionURL = jsCode2SessionURL.replace('APPID', appId).replace('SECRET', appSecret).replace('JSCODE', code);

        let jsCode2SessionResponse = await axios.get(jsCode2SessionURL);
        let sessionKey = jsCode2SessionResponse.data.session_key;
        let iv = ctx.request.body.iv;
        let encryptedData = ctx.request.body.encryptedData;
        let data = this.decode(iv, encryptedData, sessionKey, appId);
        let unionId = data.unionId;
        //当用户不存在时
        if (!await ctx.model.Userinfo.isUserExist(unionId)) {
            await ctx.model.Userinfo.createUser(data);

            //如果带有邀请
            if (ctx.request.body.invitationId) {
                await service.invite.completeInvitation(ctx.request.body.invitationId, unionId);
            }
        }

        //签发jwt
        if (await ctx.model.Userinfo.isUserExist(unionId)) {
            token = this.issueJWT(unionId, platform)
        }
        return token;
    }

    async getMobileLoginToken() {
        const { ctx, app } = this;
        let token;

        const platform = ctx.header.platform;
        let accessTokenURL = app.config.accessTokenURL;
        let code = ctx.request.body.code;
        let appId = app.config.mobile.id;
        let appSecret = app.config.mobile.secret;
        accessTokenURL = accessTokenURL.replace('APPID', appId).replace('SECRET', appSecret).replace('CODE', code);

        let accessTokenResponse = await axios.get(accessTokenURL);
        let access_token = accessTokenResponse.data.access_token;
        let openId = accessTokenResponse.data.openid;

        if (access_token !== undefined && openId !== undefined) {
            let unionIdURL = app.config.unionIdURL;
            unionIdURL = unionIdURL.replace('ACCESS_TOKEN', access_token).replace('OPENID', openId);
            let unionIdResponse = await axios.get(unionIdURL);
            let unionId = unionIdResponse.data.unionid;
            if (unionId !== undefined) {
                //用户不存在，创建新用户
                if (!await ctx.model.Userinfo.isUserExist(unionId)) {
                    let mobileUserInfo = {
                        unionId: unionIdResponse.data.unionid,
                        nickName: unionIdResponse.data.nickname,
                        gender: unionIdResponse.data.sex,
                        city: unionIdResponse.data.city,
                        avatarUrl: unionIdResponse.data.headimgurl,
                    }
                    await ctx.model.Userinfo.createUser(mobileUserInfo);
                }
                if (await ctx.model.Userinfo.isUserExist(unionId)) {
                    token = this.issueJWT(unionId, platform);
                }
            }
        }
        return token;
    }

    async getUserInfo(unionId) {
        const { ctx } = this;
        let userInfo = await ctx.model.Userinfo.getUserByUnionId(unionId);
        if (!userInfo) {
            throw new HttpCustomError(UserErrorInfo.USER_NOT_FOUND);
        }
        let accessInfo = await ctx.model.UserBetaAccess.getAccessRecordByUnionId(unionId);

        if (accessInfo) {
            return UserInfoWrapper.userCompleteInfoWrapper(userInfo, accessInfo);
        }
        else {
            return UserInfoWrapper.userCompleteInfoWrapperWithoutAccess(userInfo);
        }

    }

    async getUserInfoByMatchCode() {
        const { ctx } = this;
        let matchCode = ctx.request.query.matchCode;
        let userInfo = await ctx.model.Userinfo.getUserByMatchCode(matchCode);
        if (!userInfo) {
            throw new HttpCustomError(UserErrorInfo.USER_NOT_FOUND);
        }
        let userInfoWrapper = UserInfoWrapper.userInfoWrapper(userInfo);
        return userInfoWrapper;
    }

    async updateNickname(unionId) {
        const { ctx } = this;
        let nickname = ctx.request.body.nickname;
        await ctx.model.Userinfo.updateNickname(unionId, nickname);
    }

    async updateArea(unionId) {
        const { ctx } = this;
        let area = ctx.request.body.area;
        await ctx.model.Userinfo.updateArea(unionId, area);
    }

    async activateVIP(unionId) {
        const { ctx, app } = this;
        let code = ctx.request.body.code;

        if (code === app.config.universalKey.code) {
            //通用万能code
            let vipExpireTime = new Date(app.config.universalKey.expireTime);
            let userInfoUpdateResult = await ctx.model.Userinfo.updateVipExpireTime(unionId, vipExpireTime);

            if (userInfoUpdateResult[0] !== 1) {
                throw new HttpCustomError(UserErrorInfo.ACTIVATION_ERROR);
            }

            //公测期间准入状态
            let isAccessRecordExist = await ctx.model.UserBetaAccess.isAccessRecordExist(unionId);
            if (!isAccessRecordExist) {
                await ctx.model.UserBetaAccess.createAccessRecord(unionId, vipExpireTime);
            }
            else {
                await ctx.model.UserBetaAccess.updateAccessRecord(unionId, vipExpireTime);
            }
            
            return;
        }

        let sequenceCode = await ctx.model.SequenceCode.getNotActivatedSequenceCodeByCode(code);
        if (!sequenceCode) {
            throw new HttpCustomError(UserErrorInfo.SEQUENCE_CODE_NOT_FOUND);
        }

        let userInfo = await ctx.model.Userinfo.getUserByUnionId(unionId);
        let userVipExipreTime = new Date(userInfo.vip_expire).getTime();
        let sequenceCodeVipExpireTime = new Date(sequenceCode.vip_expire).getTime();
        //如果已经激活过, 且激活码时间少于等于现有过期时间, 不允许激活
        if (userInfo.vip_expire !== null && sequenceCodeVipExpireTime <= userVipExipreTime) {
            throw new HttpCustomError(UserErrorInfo.USER_ALREADY_ACTIVATED);
        }
        //更新激活码状态
        let sequenceCodeUpdateResult = await ctx.model.SequenceCode.assignSequenceCode(sequenceCode.code, unionId);
        if (sequenceCodeUpdateResult[0] !== 1) {
            throw new HttpCustomError(UserErrorInfo.ACTIVATION_ERROR);
        }

        //更新用户VIP状态
        let vipExpireTime = sequenceCode.vip_expire;
        await ctx.model.Userinfo.updateVipExpireTime(unionId, vipExpireTime);

        //公测期间准入状态
        let isAccessRecordExist = await ctx.model.UserBetaAccess.isAccessRecordExist(unionId);
        if (!isAccessRecordExist) {
            await ctx.model.UserBetaAccess.createAccessRecord(unionId, vipExpireTime);
        }
        else {
            await ctx.model.UserBetaAccess.updateAccessRecord(unionId, vipExpireTime);
        }

    }


    async publicBetaAccess(unionId) {
        const { ctx } = this;
        //公测期间准入资格
        let isAccessRecordExist = await ctx.model.UserBetaAccess.isAccessRecordExist(unionId);
        //未激活vip
        if (!isAccessRecordExist) {
            let tomorrow = ctx.helper.getTomorrow();
            await ctx.model.UserBetaAccess.createAccessRecord(unionId, tomorrow);
        }
    }

    

    decode(iv, encryptedData, sessionKey, appId) {
        let pc = new WXBizDataCrypt(appId, sessionKey);
        let data = pc.decryptData(encryptedData, iv);
        return data;
    }

    issueJWT(unionId, platform) {
        const { app } = this;
        let token = app.jwt.sign(
            {
                unionId: unionId,
                platform: platform
            },
            app.config.jwt.secret,
            app.config.jwt.options);

        return token;
    }

}

module.exports = UserService;
