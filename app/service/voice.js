const Service = require('egg').Service;
const OSS = require('ali-oss');
const uuidv4 = require('uuid/v4');
const VoiceErrorInfo = require('../error/errorInfo/voice');
const HttpCustomError = require('../error/HttpCustomError');

class VoiceService extends Service {
    //每日推送的官方语音
    async officialVoicePush() {
        const { ctx, app } = this;
        let date = new Date();
        let currentTime = date.getTime();
        let currentDate = ctx.helper.dateFormater(date);
        let tomorrow = ctx.helper.getTomorrow()
        let tomorrowZeroHour = new Date(`${ctx.helper.dateFormater(tomorrow)} 00:00:00`).getTime();
        let expireTimestamp = Math.ceil((tomorrowZeroHour - currentTime) / 1000);
        let IsOrNotOfficialPushToday = await this.IsOrNotOfficialPushToday();
        if (IsOrNotOfficialPushToday == true) {
            let officialVoicePush = await ctx.model.OfficialVoice.officialVoicePush(currentDate);
            let format = officialVoicePush.format;
            if (format == 'mp3') {
                let obj = { roomId: null, officialVoicePush };
                let todayPush = JSON.stringify(obj);
                await app.redis.set(`officialVoicePush@${currentDate}`, todayPush, 'ex', expireTimestamp);
                return { roomId: null, officialVoicePush };
            } else {
                let audioId = officialVoicePush.audioId;
                let authorUnionIdAndDuration = await ctx.model.UploadAudio.getAuthorUnionIdAndDurationByAudioId(audioId);
                let authorUnionId = authorUnionIdAndDuration.unionId;
                let userinfo = await ctx.model.Userinfo.getUserByUnionId(authorUnionId);
                if (userinfo == null) {
                    let authorNickname = await this.getAuthorNicknameByVoiceFormat(audioId, authorUnionId);
                    let obj = { roomId: userinfo, authorNickname, officialVoicePush };
                    let todayPush = JSON.stringify(obj);
                    await app.redis.set(`officialVoicePush@${currentDate}`, todayPush, 'ex', expireTimestamp);
                    return obj;
                } else {
                    let authorNickname = await this.getAuthorNicknameByVoiceFormat(audioId, authorUnionId);
                    let roomId = userinfo.match_code;
                    let obj = { roomId, authorNickname, officialVoicePush };
                    let todayPush = JSON.stringify(obj);
                    await app.redis.set(`officialVoicePush@${currentDate}`, todayPush, 'ex', expireTimestamp);
                    return { roomId, authorNickname, officialVoicePush };
                };
            };
        } else {
            let officialVoicePush = await this.app.redis.get(`officialVoicePush@${currentDate}`);
            return JSON.parse(officialVoicePush);
        };
    };

    //查看往期的所有语音
    async reviewOldVoices() {
        const { ctx } = this;
        let date = new Date();
        let currentDate = ctx.helper.dateFormater(date)
        let oldVoices = await this.ctx.model.OfficialVoice.reviewOldVoices(currentDate);
        if (oldVoices.length == 0) {
            throw new HttpCustomError(VoiceErrorInfo.NO_OLD_VIOICES);
        } else {
            return oldVoices;
        };
    };


    //判断用户今天有没有调用过此接口
    async onceEveryday(unionId) {
        const { app, ctx } = this;
        const date = new Date();
        let currentTime = date.getTime();
        let tomorrow = ctx.helper.getTomorrow()
        let tomorrowZeroHour = new Date(`${ctx.helper.dateFormater(tomorrow)} 00:00:00`).getTime();
        let expireTimestamp = Math.ceil((tomorrowZeroHour - currentTime) / 1000);
        if (await app.redis.get(`VipVoicePush@${unionId}`) === null) {
            await app.redis.set(`VipVoicePush@${unionId}`, unionId, 'ex', expireTimestamp);
            return true;
        }
    };

    //判断今天有没有调用过精选语音接口
    async IsOrNotOfficialPushToday() {
        const { app, ctx } = this;
        const date = new Date();
        let currentTime = date.getTime();
        let currentDate = ctx.helper.dateFormater(date);
        let tomorrow = ctx.helper.getTomorrow();
        let tomorrowZeroHour = new Date(`${ctx.helper.dateFormater(tomorrow)} 00:00:00`).getTime();
        let expireTimestamp = Math.ceil((tomorrowZeroHour - currentTime) / 1000);
        if (await app.redis.get(`officialVoicePush@${currentDate}`) === null) {
            await app.redis.set(`officialVoicePush@${currentDate}`, 'nothing here', 'ex', expireTimestamp);
            return true;
        }
    };

    //VIP的每日推送
    async vipVoicePush(unionId) {
        const { ctx } = this;
        let date = new Date();
        let currentTime = date.getTime();
        let tomorrow = ctx.helper.getTomorrow()
        let tomorrowZeroHour = new Date(`${ctx.helper.dateFormater(tomorrow)} 00:00:00`).getTime();
        let expireTimestamp = Math.ceil((tomorrowZeroHour - currentTime) / 1000);
        const today = ctx.helper.dateFormater(date);
        let isTheFirstTimeToday = await this.onceEveryday(unionId);
        if (isTheFirstTimeToday == true) {//今天第一次
            let vipHistory = await ctx.model.VipHistory.vipRecords(unionId);
            if (vipHistory.length !== 0) {
                let historyArray = new Array();
                for (let i = 0; i < vipHistory.length; i++) {
                    historyArray.push(vipHistory[i].voiceId);
                };
                let history = historyArray;
                let vipVoicePush = await this.ctx.model.VipVoicePush.vipPushWithHistory(history);
                let voiceId = vipVoicePush.voiceId;
                let audioId = vipVoicePush.audioId;
                let authorNickname = vipVoicePush.authorNickname;
                let duration = vipVoicePush.duration;
                let title = vipVoicePush.title;
                let format = vipVoicePush.format;
                if (format == 'm4a') {
                    let authorUnionIdAndDuration = await ctx.model.UploadAudio.getAuthorUnionIdAndDurationByAudioId(audioId);
                    let authorUnionId = authorUnionIdAndDuration.unionId;
                    let duration = authorUnionIdAndDuration.duration;
                    let userinfo = await ctx.model.Userinfo.getUserByUnionId(authorUnionId);
                    let authorNickname = await this.getAuthorNicknameByVipVoiceFormat(audioId, authorUnionId);
                    if (userinfo == null) {
                        let m4aVipPush = { roomId: userinfo, authorNickname, voiceId, audioId, duration, format, title };
                        let VipTodayPush = JSON.stringify(m4aVipPush);
                        await this.app.redis.set(`VipVoicePush@${unionId}`, VipTodayPush, 'ex', expireTimestamp);
                        await this.ctx.model.VipHistory.record(unionId, voiceId, today);
                        return m4aVipPush;
                    } else {
                        let roomId = userinfo.match_code;
                        let m4aVipPush = { roomId, authorNickname, voiceId, audioId, duration, format, title };
                        let VipTodayPush = JSON.stringify(m4aVipPush);
                        await this.app.redis.set(`VipVoicePush@${unionId}`, VipTodayPush, 'ex', expireTimestamp);
                        await this.ctx.model.VipHistory.record(unionId, voiceId, today);
                        return m4aVipPush;
                    };
                } else {
                    let mp3VipPush = { roomId: null, authorNickname, voiceId, audioId, duration, format, title };
                    let VipTodayPush = JSON.stringify(mp3VipPush);
                    await this.app.redis.set(`VipVoicePush@${unionId}`, VipTodayPush, 'ex', expireTimestamp);
                    await this.ctx.model.VipHistory.record(unionId, voiceId, today);
                    return mp3VipPush;
                };
            } else {
                let vipVoicePush = await this.ctx.model.VipVoicePush.vipPushWithNoHistory();
                let voiceId = vipVoicePush.voiceId;
                let audioId = vipVoicePush.audioId;
                let authorNickname = vipVoicePush.authorNickname;
                let duration = vipVoicePush.duration;
                let title = vipVoicePush.title;
                let format = vipVoicePush.format;
                if (format == 'm4a') {
                    let authorUnionIdAndDuration = await ctx.model.UploadAudio.getAuthorUnionIdAndDurationByAudioId(audioId);
                    let authorUnionId = authorUnionIdAndDuration.unionId;
                    let duration = authorUnionIdAndDuration.duration;
                    let userinfo = await ctx.model.Userinfo.getUserByUnionId(authorUnionId);
                    let authorNickname = await this.getAuthorNicknameByVipVoiceFormat(audioId, authorUnionId);
                    if (userinfo == null) {
                        let m4aVipPush = { roomId: userinfo, authorNickname, voiceId, audioId, duration, format, title };
                        let VipTodayPush = JSON.stringify(m4aVipPush);
                        await this.app.redis.set(`VipVoicePush@${unionId}`, VipTodayPush, 'ex', expireTimestamp);
                        await this.ctx.model.VipHistory.record(unionId, voiceId, today);
                        return m4aVipPush;
                    } else {
                        let roomId = userinfo.match_code;
                        let m4aVipPush = { roomId, authorNickname, voiceId, audioId, duration, format, title };
                        let VipTodayPush = JSON.stringify(m4aVipPush);
                        await this.app.redis.set(`VipVoicePush@${unionId}`, VipTodayPush, 'ex', expireTimestamp);
                        await this.ctx.model.VipHistory.record(unionId, voiceId, today);
                        return m4aVipPush;
                    };
                } else {
                    let mp3VipPush = { roomId: null, authorNickname, voiceId, audioId, duration, format, title };
                    let VipTodayPush = JSON.stringify(mp3VipPush);
                    await this.app.redis.set(`VipVoicePush@${unionId}`, VipTodayPush, 'ex', expireTimestamp);
                    await this.ctx.model.VipHistory.record(unionId, voiceId, today);
                    return mp3VipPush;
                };
            };
        } else {//不是今天第一次，查找今天数据库里的语音
            let VipTodayPush = await this.app.redis.get(`VipVoicePush@${unionId}`);
            return JSON.parse(VipTodayPush);
        };

    };


    //返回作者昵称（精选语音表）
    async getAuthorNicknameByVoiceFormat(audioId, unionId) {
        let formatJudgement = await this.ctx.model.OfficialVoice.formatJudgement(audioId);
        if (formatJudgement.format == 'mp3') {//该语音是从公众号征集的
            let findAuthorNickname = await this.ctx.model.OfficialVoice.findAuthorNickname(audioId);
            return findAuthorNickname.authorNickname;
        } else {//该条语音是从小程序征集的
            let authorMessage = await this.ctx.model.Userinfo.getUserByUnionId(unionId);
            if (authorMessage == null) {
                return null;//没有这位小程序用户
            } else {
                let nickname = authorMessage.nickname;
                return nickname;
            };
        };

    };

    //返回作者昵称（VIP语音表）
    async getAuthorNicknameByVipVoiceFormat(audioId, unionId) {
        let formatJudgement = await this.ctx.model.VipVoicePush.formatJudgement(audioId);
        if (formatJudgement.format == 'mp3') {//该语音是从公众号征集的
            let findAuthorNickname = await this.ctx.model.VipVoicePush.findAuthorNickname(audioId);
            return findAuthorNickname.authorNickname;
        } else {//该条语音是从小程序征集的
            let authorMessage = await this.ctx.model.Userinfo.getUserByUnionId(unionId);
            if (authorMessage == null) {
                return null;//没有这位小程序用户
            } else {
                let nickname = authorMessage.nickname;
                return nickname;
            };
        };

    };



    async uploadAudio(unionId) {
        const { ctx, app } = this;
        const DAILY_UPLOAD_LIMIT = 3;
        let client = new OSS({
            region: app.config.alioss.region,
            accessKeyId: app.config.alioss.accessKeyId,
            accessKeySecret: app.config.alioss.accessKeySecret,
            bucket: app.config.alioss.bucket
        });
        let dayWindow = {
            beginTime: `${ctx.helper.dateFormater(new Date())} 00:00:00`,
            endTime: `${ctx.helper.dateFormater(new Date())} 23:59:59`
        }
        let audioDailyUploadCount = await ctx.model.UploadAudio.getAudioDailyUploadCount(unionId, dayWindow);
        //每日上传限制
        if (audioDailyUploadCount >= DAILY_UPLOAD_LIMIT) {
            throw new HttpCustomError(VoiceErrorInfo.DAILY_AUDIO_COUNT_EXCEED_LIMIT);
        }
        let stream = await ctx.getFileStream();
        let filename = stream.filename;
        let extension = filename.substring(filename.length - 3, filename.length);
        let duration = ctx.request.query.duration;
        let audioId = uuidv4();
        let uploadResult = await client.putStream(`user_upload_audio/${audioId}.${extension}`, stream);
        if (uploadResult.res.status === 200) {
            let audio = {
                audioId: audioId,
                unionId: unionId,
                duration: duration,
            }
            await ctx.model.UploadAudio.createUploadAudio(audio);
        }
        else {
            throw new HttpCustomError(VoiceErrorInfo.AUDIO_UPLOAD_ERROR);
        }

    }
};
module.exports = VoiceService;