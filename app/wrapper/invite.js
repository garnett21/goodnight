module.exports = {
    inviteeWrapper(invitee) {
        let wrapper = {
            nickname: invitee.nickname,
            avatar: invitee.avatar,
        }
        return wrapper;
    },

    invitationWrapper(invitation, invitee) {
        let wrapper = {
            invitationId: invitation.invitationId,
            completed: invitation.completed,
            expireAt: invitation.expireAt,
            invitee: invitee
        }
        return wrapper;
    }
};
