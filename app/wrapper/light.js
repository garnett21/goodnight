module.exports = {
    lightWrapper(userLight, lightInfo) {
        let wrapper = {
            lightId: userLight.lightId,
            lightStatus: userLight.status,
            lightInfo: lightInfo
        }
        return wrapper;
    },
};
