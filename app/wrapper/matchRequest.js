module.exports = {
    //收到的匹配邀请(http)
    receivedRequestWrapper(matchRequest, senderInfo) {
        let wrapper = {
            requestId: matchRequest.requestId,
            senderId: matchRequest.senderId,
            senderNickname: senderInfo.nickname,
            senderGender: senderInfo.gender,
            senderAvatar: senderInfo.avatar,
            senderMatchCode: senderInfo.match_code,
            status: matchRequest.status,
            createdAt: matchRequest.createdAt,
            expireAt: matchRequest.expireAt,
        }
        return wrapper;
    },

    //发出的匹配邀请(http)
    sentRequestWrapper(matchRequest, receiverInfo) {
        let wrapper = {
            requestId: matchRequest.requestId,
            receiverId: matchRequest.senderId,
            receiverNickname: receiverInfo.nickname,
            receiverGender: receiverInfo.gender,
            receiverAvatar: receiverInfo.avatar,
            receiverMatchCode: receiverInfo.match_code,
            status: matchRequest.status,
            createdAt: matchRequest.createdAt,
            expireAt: matchRequest.expireAt,
        }
        return wrapper;
    },

    //回复匹配邀请(socket)
    replyRequestWrapper(requestId, receiverInfo) {
        let wrapper = {
            requestId: requestId,
            receiverNickname: receiverInfo.nickname,
            receiverGender: receiverInfo.gender,
            receiverAvatar: receiverInfo.avatar,
            receiverMatchCode: receiverInfo.match_code
        }
        return wrapper;
    },

    //发出匹配邀请(socket)
    sendRequestWrapper(requestId, senderInfo) {
        let wrapper = {
            requestId: requestId,
            senderNickname: senderInfo.nickname,
            senderGender: senderInfo.gender,
            senderAvatar: senderInfo.avatar,
            senderMatchCode: senderInfo.match_code
        }
        return wrapper;
    }
};
