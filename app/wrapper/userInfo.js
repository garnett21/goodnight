module.exports = {
    matchUserInfoWrapper(userinfo, light) {
        let wrapper = {
            nickname: userinfo.nickname,
            gender: userinfo.gender,
            avatar: userinfo.avatar,
            matchCode: userinfo.match_code,
            area: userinfo.area,
            light: light
        }
        return wrapper;
    },

    userInfoWrapper(userinfo) {
        let wrapper = {
            nickname: userinfo.nickname,
            gender: userinfo.gender,
            avatar: userinfo.avatar,
            matchCode: userinfo.match_code
        }
        return wrapper;
    },

    userCompleteInfoWrapper(userInfo, accessInfo) {
        let wrapper = {
            unionId: userInfo.unionId,
            nickname: userInfo.nickname,
            gender: userInfo.gender,
            vip_expire: userInfo.vip_expire,
            area: userInfo.area,
            avatar: userInfo.avatar,
            match_code: userInfo.match_code,
            accessExpire: accessInfo.expireAt,
        }
        return wrapper;
    },

    userCompleteInfoWrapperWithoutAccess(userInfo) {
        let wrapper = {
            unionId: userInfo.unionId,
            nickname: userInfo.nickname,
            gender: userInfo.gender,
            vip_expire: userInfo.vip_expire,
            area: userInfo.area,
            avatar: userInfo.avatar,
            match_code: userInfo.match_code,
        }
        return wrapper;
    }
};
