module.exports = {
    voiceMessageWrapper(message) {
        let wrapper = {
            messageId: message.messageId,
            duration:message.duration,
            createdAt: message.createdAt,
        }
        return wrapper;
    }
};
