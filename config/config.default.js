'use strict';

module.exports = appInfo => {
  const config = exports = {};

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1541399842851_2420';

  // add your config here
  config.middleware = ['errorHandler'];

  config.jsCode2SessionURL = 'https://api.weixin.qq.com/sns/jscode2session?appid=APPID&secret=SECRET&js_code=JSCODE&grant_type=authorization_code';
  config.accessTokenURL = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code';
  config.unionIdURL = 'https://api.weixin.qq.com/sns/userinfo?access_token=ACCESS_TOKEN&openid=OPENID';

  config.miniapp = {
    id: 'wx6f997e1f2ca86b55',
    secret: '7ff303108ed76a5d2201f529a66c187c',
  }

  config.mobile = {
    id: 'wx5549d8dd8a9697f2',
    secret: 'dd7d2e94b1ebb988f5e85290542452c0'
  }

  config.security = {
    csrf: {
      enable: false,
    },
  };

  config.cors = {
    origin: '*',
    allowMethods: 'GET,PUT,POST'

  };

  config.onerror = {
    accepts() {
      return 'json';
    },
  }

  config.logrotator = {
    maxDays: 31,                     // keep max days log files, default is `31`. Set `0` to keep all logs
  };

  config.io = {
    init: {}, // passed to engine.io
    namespace: {
      '/': {
        connectionMiddleware: ['auth'],
        packetMiddleware: ['errorHandler'],
      },
    },
  };


  config.universalKey = {
    code: '#wyn3411',
    expireTime: '2099-12-31 23:59:59'
  }

  config.baseURL = '/api/v1';

  //1分钟,毫秒
  config.matchTTL = 60000;

  config.socketChannel = {
    invalidToken: 'invalidToken',
    randomMatch: 'randomMatch',
    sendMatchRequest: 'sendMatchRequest',
    replyMatchRequest: 'replyMatchRequest',
    newVoiceMessage: 'newVoiceMessage',
    matchLightOff: 'matchLightOff',
  }

  config.multipart = {
    fileExtensions: ['.m4a']
  };

  config.alioss = {
    accessKeyId: 'LTAIX9AQ6J5XlQNV',
    accessKeySecret: '0XXjtUwuatQdNNMcl9nWhYxhJgSq2X',
    bucket: 'wyn-goodnight',
    region: 'oss-cn-shenzhen'
  }

  return config;
};
