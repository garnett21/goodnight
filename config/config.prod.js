'use strict';
const path = require('path');
module.exports = appInfo => {
  const config = exports = {};

  config.logger = {
    dir: path.join(appInfo.baseDir, 'logs'),
  }
  
  config.sequelize = {
    dialect: 'mysql', // support: mysql, mariadb, postgres, mssql
    database: 'goodnight_prod',
    host: 'rm-wz96015e01vpo7907622.mysql.rds.aliyuncs.com',
    port: '3306',
    username: 'goodnight',
    password: 'WYN_goodnight',
    dialectOptions: {
      useUTC: false, //for reading from database
      dateStrings: true,

      typeCast: function (field, next) { // for reading from database
        if (field.type === 'DATETIME') {
          return field.string()
        }
        return next()
      },
    },
    timezone: '+08:00'
  }

  config.jwt = {
    secret: "WhatYouNeed_3411@2018",
    options: {
      expiresIn: '2h'
    }
  };

  config.matchWindow = {
    beginTime: '22:00:00',
    endTime: '10:00:00',
  }

  config.refreshWindow = {
    beginTime: '22:00:00',
    endTime: '21:59:30',
  }

  config.lightWindow = {
    beginTime: '22:00:00',
    endTime: '05:30:00',
  },

  config.schedule = {
    deactivatePendingRequest: {
      //21点59分40秒执行一次
      cron: '40 59 21 * * *',
      type: 'worker',
      disable: false
    },

    renewCurrentMatchRequest: {
      //21点59分50秒执行一次
      cron: '50 59 21 * * *',
      type: 'worker',
      disable: false,
    },

    autoTurnOnLight: {
      //每天这个时间自动开灯
      cron: '0 30 5 * * *',
      type: 'worker',
      disable: false,
    }

  };

  config.redis = {
    client: {
      port: 6379,          // Redis port
      host: 'r-wz94d470447cd924.redis.rds.aliyuncs.com',   // Redis host
      password: '',
      db: 34,
    },
  }

  config.alinode = {
    server: 'wss://agentserver.node.aliyun.com:8080',
    appid: '77322',
    secret: '848da5ce1e17bfaaafbe3dbad6d47d4187c6744c',
    logdir: '/tmp/',
    agentidMode:'IP'
  };

  return config;
};