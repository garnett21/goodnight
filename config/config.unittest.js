'use strict';

module.exports = appInfo => {
  const config = exports = {};

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1532075749704_9610';

  config.sequelize = {
    dialect: 'mysql', // support: mysql, mariadb, postgres, mssql
    database: 'goodnight_test',
    host: 'rm-wz96015e01vpo7907622.mysql.rds.aliyuncs.com',
    port: '3306',
    username: 'goodnight',
    password: 'WYN_goodnight',
    dialectOptions: {
      useUTC: false, //for reading from database
      dateStrings: true,

      typeCast: function (field, next) { // for reading from database
        if (field.type === 'DATETIME') {
          return field.string()
        }
        return next()
      },
    },
    timezone: '+08:00'
  }

  config.jwt = {
    secret: "WhatYouNeed_3411@2018",
    options: {
      expiresIn: '2h'
    }
  };

  config.matchWindow = {
    beginTime: '22:00:00',
    endTime: '10:00:00',
  }

  config.refreshWindow = {
    beginTime: '22:00:00',
    endTime: '21:59:30',
  }
  
  // config.matchWindow = {
  //   beginTime: '11:00:00',
  //   endTime: '10:00:00',
  // }

  // config.refreshWindow = {
  //   beginTime: '11:00:00',
  //   endTime: '10:59:30',
  // }

  config.lightWindow = {
    beginTime: '11:00:00',
    endTime: '10:00:00',
  },

  config.schedule = {
    deactivatePendingRequest: {
      //21点59分40秒执行一次
      cron: '40 59 21 * * *',
      type: 'worker',
      disable: false
    },

    renewCurrentMatchRequest: {
      //21点59分50秒执行一次
      cron: '50 59 21 * * *',
      type: 'worker',
      disable: false,
    },

    autoTurnOnLight: {
      //每天这个时间自动开灯
      cron: '0 0 13 * * *',
      type: 'worker',
      disable: false,
    }

  };
  config.redis = {
    client: {
      port: 6379,          // Redis port
      host: 'r-wz94d470447cd924.redis.rds.aliyuncs.com',   // Redis host
      password: '',
      db: 33,
    },
  }

  config.alinode = {
    server: 'wss://agentserver.node.aliyun.com:8080',
    appid: '77474',
    secret: '41bbe7ed74e224647491d8cb6a95c09f2274a38f',
    logdir: '/tmp/',
    agentidMode: 'IP'
  };

  return config;
};

