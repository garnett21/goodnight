'use strict';

// had enabled by egg
// exports.static = true;
exports.jwt = {
  enable: true,
  package: "egg-jwt"
};

exports.cors = {
  enable: true,
  package: 'egg-cors',
};

exports.validate = {
  enable: true,
  package: 'egg-validate',
};

exports.sequelize = {
  enable: true,
  package: 'egg-sequelize'
}

exports.logrotator = {
  enable: true,
  package: 'egg-logrotator',
};

exports.redis = {
  enable: true,
  package: 'egg-redis',
};

exports.io = {
  enable: true,
  package: 'egg-socket.io',
};

exports.alinode = {
  enable: true,
  package: 'egg-alinode',
  env: ['prod', 'unittest']
};